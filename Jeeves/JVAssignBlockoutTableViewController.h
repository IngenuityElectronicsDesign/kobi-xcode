//
//  JVAssignBlockoutTableViewController.h
//  Jeeves
//
//  Created by Simon on 28/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVAssignBlockoutTableViewController : UITableViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *blockoutButtons;

@property (strong, nonatomic) IBOutlet UITextField *startTimeHours;
@property (strong, nonatomic) IBOutlet UITextField *startTimeMinutes;
@property (strong, nonatomic) IBOutlet UITextField *endTimeHours;
@property (strong, nonatomic) IBOutlet UITextField *endTimeMinutes;

@end
