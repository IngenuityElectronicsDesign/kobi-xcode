//
//  JVDataSyncTableViewController.m
//  Kobi
//
//  Created by Simon on 10/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVDataSyncTableViewController.h"

@interface JVDataSyncTableViewController ()

@property (nonatomic, strong) UIBarButtonItem* saveButton;
@property (nonatomic, strong) UIBarButtonItem* discardButton;

@end

@implementation JVDataSyncTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    if( self = [super initWithStyle:style] )
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed)];
    _discardButton = [[UIBarButtonItem alloc] initWithTitle:@"Discard" style:UIBarButtonItemStylePlain target:self action:@selector(discardButtonPressed)];
}

- (void)dataModified:(BOOL)modified
{
    if( [self navigationController] )
    {
        if( modified )
        {
            if( [[self navigationItem] rightBarButtonItem] != _saveButton )
            {
                [[self navigationItem] setRightBarButtonItem:_saveButton animated:YES];
                [[self navigationItem] setLeftBarButtonItem:_discardButton animated:YES];
            }
        }
        else
        {
            if( [[self navigationItem] rightBarButtonItem] == _saveButton )
            {
                [[self navigationItem] setLeftBarButtonItem:nil animated:YES];
                [[self navigationItem] setRightBarButtonItem:nil animated:YES];
            }
        }
    }
}

- (void)saveButtonPressed
{
    NSLog(@"saveButtonPressed not overridden");
}

- (void)discardButtonPressed
{
    NSLog(@"discardButtonPressed not overridden");
}

@end
