//
//  JVTimeInputCell.h
//  Kobi
//
//  Created by Simon on 14/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardManager.h"

@protocol JVTimeInputCellDelegate <NSObject>

- (void)timeValueDidChangeToQuotaChar:(unsigned char)quotaChar;

@end

@interface JVTimeInputCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, weak) id<JVTimeInputCellDelegate> delegate;

@property (nonatomic) int maxHours;

@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UILabel* seperatorLabel;
@property (nonatomic, strong) UITextField* hourTextField;
@property (nonatomic, strong) UITextField* minuteTextField;

@property (nonatomic) int hourValue;
@property (nonatomic) int minuteValue;

- (void)setupCellWithTitle:(NSString*)title;
- (void)setupCellWithTitle:(NSString*)title andMaxHours:(int)maxHours;

+ (unsigned char)quotaCharFromHours:(int)hours andMinutes:(int)mins;
+ (void)hours:(int*)hours andMinutes:(int*)mins fromQuotaChar:(unsigned char)quotaChar;

@end
