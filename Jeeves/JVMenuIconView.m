//
//  JVMenuIconView.m
//  Jeeves
//
//  Created by Simon on 9/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVMenuIconView.h"

@implementation JVMenuIconView

- (id)initWithFrame:(CGRect)frame title:(NSString*)title image:(UIImage*)image andIndex:(int)index
{
    if( self = [super initWithFrame:frame andIndex:index] )
    {
        [[super button] setImage:[image imageWithTintColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] loggedInUser] tintColorIndex]]] forState:UIControlStateNormal];
        
        [[[super button] imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] loggedInUser] tintColorIndex]]];
        
        [[super label] setText:title];
    }
    return self;
}

@end
