//
//  JVDeviceClass.m
//  Kobi
//
//  Created by Simon on 17/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVDeviceClassData.h"

@implementation JVDeviceClassData

- (id)initWithName:(NSString *)name andIcon:(UIImage *)icon
{
    if( self = [super init] )
    {
        _name = name;
        _icon = icon;
    }
    return self;
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVDeviceClassData* other = otherData;
        
        if( ![[self name] isEqualToString:[other name] ] )
        {
            [conflicts addObject:@(DeviceClassConflictType_NAME)];
            isEqual = NO;
        }
        if( [self icon] != [other icon] )
        {
            [conflicts addObject:@(DeviceClassConflictType_ICON)];
            isEqual = NO;
        }
        
        return isEqual;
    }
    return NO;
}

@end
