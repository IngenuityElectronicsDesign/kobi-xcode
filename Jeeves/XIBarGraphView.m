//
//  XIBarGraphView.m
//  eXtended Interface
//
//  Created by Simon on 14/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//

#import "XIBarGraphView.h"

@interface XIBarGraphView ()

@property (nonatomic) int leftPadding;
@property (nonatomic) int bottomPadding;

@property (nonatomic) int horizSpacing;

@property (nonatomic, strong) NSMutableArray* barGraphViews;

@end

@implementation XIBarGraphView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self viewDidInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if( self )
    {
        [self viewDidInit];
    }
    return self;
}

- (void)viewDidInit
{
    _barGraphViews = [[NSMutableArray alloc] init];
    
    _leftPadding = 32;//24;
    _bottomPadding = 22;//24;
    
    _horizSpacing = 10;
}

- (void)drawBarsInGraphWithDataArray:(NSArray*)dataArray withMaxHeight:(NSNumber *)maxHeight
{
    [self clearBarsInGraph];
    
    int barWidth = (([self bounds].size.width - _leftPadding) - (_horizSpacing * dataArray.count + 1))/dataArray.count;
    float largestNumber;
    if( maxHeight > 0 )
    {
        largestNumber = [maxHeight floatValue];
    }
    else
    {
        largestNumber = [self returnLargestFloatFromArray:dataArray withPadding:1.8f];
    }
    
    for( int i = 0; i < dataArray.count; i ++ )
    {
        CGFloat barX = (_leftPadding+((i+1)*_horizSpacing)+(i*barWidth));
        CGFloat barOriginY = (self.frame.size.height-_bottomPadding);
        CGFloat barHeight = ([dataArray[i] floatValue] / largestNumber);
        
        [_barGraphViews addObject:[self drawBarInGraphWithInitalFrame:CGRectMake( barX, barOriginY, barWidth, 0 ) destinationFrame:CGRectMake( barX, barOriginY*(1-barHeight), barWidth, barOriginY*barHeight) animationDuration:@(0.5f) initalColor:[UIColor colorWithRed:70/255.0f green:130/255.0f blue:180/255.0f alpha:1.0f] andDestinationColor:barHeight < 0.75f ? nil : nil]];//[UIColor redColor]]];
    }
}

- (UIView*)drawBarInGraphWithInitalFrame:(CGRect)initalFrame destinationFrame:(CGRect)destinationFrame animationDuration:(NSNumber*)duration initalColor:(UIColor*)initialColor andDestinationColor:(UIColor*)destinationColor
{
    // Setup coloured bar view
    UIView *barGraph = [[UIView alloc] initWithFrame:initalFrame];
    [barGraph setBackgroundColor:initialColor];
    
    // Add "candy cane" lines above the coloured bar view
    UIView *lines = [[UIView alloc] initWithFrame:barGraph.bounds];
    [lines setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Lines.png"]]];
    [lines setAlpha:0.5f];
    [barGraph addSubview:lines];
    
    [self addSubview:barGraph];
    
    // Handle Animation
    [UIView animateWithDuration:[duration floatValue] delay:0.0 options:(UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction) animations:
    ^{
        [barGraph setFrame:destinationFrame];
        [lines setFrame:CGRectMake(0, 0, destinationFrame.size.width, destinationFrame.size.height)];
        if( destinationColor != nil )
        {
            [barGraph setBackgroundColor:destinationColor];
        }
    } completion:
    ^(BOOL finished){ }
    ];
    
    return barGraph;
}

- (void)clearBarsInGraph
{
    for( int i = 0; i < _barGraphViews.count; i++ )
    {
        [_barGraphViews[i] removeFromSuperview];
    }
    
    [_barGraphViews removeAllObjects];
}

- (float)returnLargestFloatFromArray:(NSArray*)array withPadding:(float)padding
{
    float largestNumber = 0.0f;
    
    for( int i = 0; i < [array count]; i++ )
    {
        if( [array[i] floatValue] > largestNumber )
        {
            largestNumber = [array[i] floatValue];
        }
    }
    
    return largestNumber + padding;
}

@end
