//
//  XIImageView.m
//  eXtended Interface
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//

#import "XIImageView.h"

@implementation UIImageView (XIImageView)

- (UIImageView *)imageCroppedToCircleWithBorderThickness:(float)borderThickness andBorderColor:(UIColor *)borderColor
{
    CALayer *imageLayer = [self layer];
    [imageLayer setCornerRadius:(self.frame.size.width/2)];
    [imageLayer setBorderWidth:borderThickness];
    [imageLayer setBorderColor:[borderColor CGColor]];
    [imageLayer setMasksToBounds:YES];
    
    return self;
}

@end
