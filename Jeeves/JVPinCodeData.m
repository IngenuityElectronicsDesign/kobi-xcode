//
//  JVPinCode.m
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVPinCodeData.h"

#define kPinCodeLength 4

@implementation JVPinCodeData

- (id)init
{
    if( self = [super init] )
    {
        _pin = 27; // Default to "1234"
    }
    return self;
}

- (id)initWithPinString:(NSString*)pinString
{
    if( self = [super init] )
    {
        [self setPinWithString:pinString];
    }
    return self;
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVPinCodeData* other = otherData;
        
        if( [self pin] != [other pin] )
        {
            [conflicts addObject:@(PinCodeConflictType_PIN)];
            isEqual = NO;
        }
        
        return isEqual;
    }
    return NO;
}

- (BOOL)isEqualToPinCode:(JVPinCodeData *)aPinCode
{
    return [self pin] == [aPinCode pin];
}

- (NSString*)getPinAsString
{
    char digits[kPinCodeLength+1];
    
    for( int i = 0; i < kPinCodeLength; i++ )
    {
        digits[i] = (_pin >> (6-i*2));
        
        digits[i] = (digits[i] & 0x03) + 0x31;
        
        switch( digits[i] )
        {
            case 0:
            {
                digits[i] = '0';
                break;
            }
            case 1:
            {
                digits[i] = '1';
                break;
            }
            case 2:
            {
                digits[i] = '2';
                break;
            }
            case 3:
            {
                digits[i] = '3';
                break;
            }
        }
    }
    
    digits[kPinCodeLength] = (char)NULL;
    
    return [NSString stringWithCString:digits encoding:NSASCIIStringEncoding];
}

- (void)setPinWithString:(NSString *)pinString
{
    // Ensure length is 4
    if( [pinString length] != kPinCodeLength )
    {
        [[NSException exceptionWithName:@"JVPinCode pinString length exception" reason:@"pinString was not 4 chars long" userInfo:nil] raise];
    }
    
    unsigned char digits[kPinCodeLength];
    
    // Ensure component chars are in the range '1' to '4' (0 to 3 after subtraction)
    for( int i = 0; i < kPinCodeLength; i++ )
    {
        digits[i] = [pinString characterAtIndex:i] - 0x31;
        
        if( digits[i] < 0 || digits[i] > 3 )
        {
            [[NSException exceptionWithName:@"JVPinCode pinString digit exception" reason:[NSString stringWithFormat:@"pinString digit %d had a value %d which is not in the range 1-4", i+1, digits[i]+1] userInfo:nil] raise];
        }
    }
    
    // Assign chars to pin after bitshifting
    _pin = ( (digits[0] << 6) | (digits[1] << 4) | (digits[2] << 2) | (digits[3] << 0) );
}

@end
