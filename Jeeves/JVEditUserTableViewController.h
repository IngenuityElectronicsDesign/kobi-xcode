//
//  JVEditUserTableViewController.h
//  Kobi
//
//  Created by Simon on 9/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVDataSyncTableViewController.h"
#import "XIImageView.h"

@interface JVEditUserTableViewController : JVDataSyncTableViewController <UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>

@property (nonatomic) BOOL isAdmin;
@property (nonatomic, strong) JVUserData* selectedUser;
@property (nonatomic, strong) JVUserData* modifiedUser;

@property (nonatomic, strong) NSIndexPath* selectedIndexPath;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UIButton *portraitButton;

@property (strong, nonatomic) IBOutlet UITableViewCell *pinCell;
@property (nonatomic) BOOL pinIsShowing;

@property (strong, nonatomic) IBOutlet UILabel *removeLabel;

@property (nonatomic, strong) UIActionSheet* imagePickerActionSheet;
@property (nonatomic, strong) UIImagePickerController* imagePickerController;

@property (nonatomic, strong) UIActionSheet* removeUserActionSheet;

@property (nonatomic, strong) UIAlertView* inputErrorAlertView;

@end
