//
//  UIColor+XIColor.m
//  eXtented Interface
//
//  Created by Simon on 1/11/2013.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "UIColor+XIColor.h"

@implementation UIColor (XIColor)

#pragma mark - UIColor from Hex

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
	
    [scanner scanHexInt:&rgbValue];
	
    return [UIColor colorWithHexNumber:rgbValue andAlpha:1.0];
}

+ (UIColor *) colorWithHexNumber:(UInt32)hex andAlpha:(CGFloat)alpha {
    return [UIColor colorWithRed:((hex >> 16) & 0xFF) / 255.0
                           green:((hex >> 8) & 0xFF) / 255.0
                            blue:(hex & 0xFF) / 255.0
                           alpha:alpha];
}

#pragma mark - UIColor from Array

+ (UIColor *)colorWithRGBAArray:(NSArray *)rgbaArray {
    // Takes an array of RGBA int's, and makes a UIColor (shorthand colorWithRed:Green:Blue:Alpha:
    int r = [rgbaArray [0] floatValue]/255.0;
    int g = [rgbaArray [1] floatValue]/255.0;
    int b = [rgbaArray [2] floatValue]/255.0;
    int a = [rgbaArray [3] floatValue];
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

+ (UIColor *)colorWithCMYK:(NSArray *)cmykValues{
    int c = [cmykValues[0] floatValue];
    int m = [cmykValues[1] floatValue];
    int y = [cmykValues[2] floatValue];
    int k = [cmykValues[3] floatValue];
    
    NSArray *rgbaArray = @[[NSNumber numberWithFloat:255 * (1-c) * (1-k)],
                           [NSNumber numberWithFloat:255 * (1-m) * (1-k)],
                           [NSNumber numberWithFloat:255 * (1-y) * (1-k)],
                           [NSNumber numberWithFloat:1]];
    
    return [self colorWithRGBAArray:rgbaArray];
}

#pragma mark - Hex from UIColor

- (NSString *)hexString
{
    NSArray *colorArray	= [self rgbaArray];
    int r = [colorArray[0] floatValue] * 255;
    int g = [colorArray[1] floatValue] * 255;
    int b = [colorArray[2] floatValue] * 255;
    NSString *red = [NSString stringWithFormat:@"%02x", r];
    NSString *green = [NSString stringWithFormat:@"%02x", g];
    NSString *blue = [NSString stringWithFormat:@"%02x", b];
	
    return [NSString stringWithFormat:@"#%@%@%@", red, green, blue];
}

#pragma mark - RGBA from UIColor

- (NSArray *)rgbaArray
{
    // Takes a UIColor and returns R,G,B,A values in NSNumber form
    double r=0,g=0,b=0,a=0;
    
    if ([self respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
        [self getRed:&r green:&g blue:&b alpha:&a];
    }
    else {
        const CGFloat *components = CGColorGetComponents(self.CGColor);
        r = components[0];
        g = components[1];
        b = components[2];
        a = components[3];
    }
    return @[[NSNumber numberWithFloat:r],[NSNumber numberWithFloat:g],[NSNumber numberWithFloat:b],[NSNumber numberWithFloat:a]];
}

-(NSDictionary *)rgbaDict {
    // Takes UIColor and returns RGBA values in a dictionary as NSNumbers
    double r=0,g=0,b=0,a=0;
    if ([self respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
        [self getRed:&r green:&g blue:&b alpha:&a];
    }
    else {
        const CGFloat *components = CGColorGetComponents(self.CGColor);
        r = components[0];
        g = components[1];
        b = components[2];
        a = components[3];
    }
    
    return @{@"r":[NSNumber numberWithFloat:r], @"g":[NSNumber numberWithFloat:g], @"b":[NSNumber numberWithFloat:b], @"a":[NSNumber numberWithFloat:a]};
}

- (NSArray *)rgbaValues{
    NSArray *color = [self rgbaArray];
    double r,g,b,a;
    r = [color[0] floatValue] * 255;
    g = [color[1] floatValue] * 255;
    b = [color[2] floatValue] * 255;
    a = [color[3] floatValue];
    
    return @[[NSNumber numberWithFloat:r],
             [NSNumber numberWithFloat:g],
             [NSNumber numberWithFloat:b],
             [NSNumber numberWithFloat:a]];
}

#pragma mark - HSBA from UIColor

- (NSArray *)hsbaArray
{
    // Takes a UIColor and returns Hue,Saturation,Brightness,Alpha values in NSNumber form
    double h=0,s=0,b=0,a=0;
    
    if ([self respondsToSelector:@selector(getHue:saturation:brightness:alpha:)]) {
        [self getHue:&h saturation:&s brightness:&b alpha:&a];
    }
    
    return @[[NSNumber numberWithFloat:h],[NSNumber numberWithFloat:s],[NSNumber numberWithFloat:b],[NSNumber numberWithFloat:a]];
}

-(NSDictionary *)hsbaDict {
    // Takes a UIColor and returns Hue,Saturation,Brightness,Alpha values in NSNumber form
    double h=0,s=0,b=0,a=0;
    
    if ([self respondsToSelector:@selector(getHue:saturation:brightness:alpha:)]) {
        [self getHue:&h saturation:&s brightness:&b alpha:&a];
    }
    
    return @{@"h":[NSNumber numberWithFloat:h],@"s":[NSNumber numberWithFloat:s],@"b":[NSNumber numberWithFloat:b],@"a":[NSNumber numberWithFloat:a]};
}

#pragma mark - Generate Color Scheme

- (NSArray *)colorSchemeOfType:(ColorScheme)type
{
    NSArray *hsbArray = [self hsbaArray];
    double hue = [hsbArray[0] floatValue] * 360;
    double sat = [hsbArray[1] floatValue] * 100;
    double bright = [hsbArray[2] floatValue] * 100;
    double alpha = [hsbArray[3] floatValue];
    
    switch (type) {
        case ColorSchemeAnalagous:
            return [UIColor analagousColorsFromHue:hue saturation:sat brightness:bright alpha:alpha];
        case ColorSchemeMonochromatic:
            return [UIColor monochromaticColorsFromHue:hue saturation:sat brightness:bright alpha:alpha];
        case ColorSchemeTriad:
            return [UIColor triadColorsFromHue:hue saturation:sat brightness:bright alpha:alpha];
        case ColorSchemeComplementary:
            return [UIColor complementaryColorsFromHue:hue saturation:sat brightness:bright alpha:alpha];
        default:
            return nil;
    }
}


#pragma mark - Color Scheme Generation - Helper methods

+ (NSArray *)analagousColorsFromHue:(float)h saturation:(float)s brightness:(float)b alpha:(float)a
{
    UIColor *color = [UIColor colorWithHue:h/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorAbove1 = [UIColor colorWithHue:[UIColor addDegrees:15 toDegree:h]/360 saturation:(s-5)/100 brightness:(b-5)/100 alpha:a];
    UIColor *colorAbove2 = [UIColor colorWithHue:[UIColor addDegrees:30 toDegree:h]/360 saturation:(s-5)/100 brightness:(b-10)/100 alpha:a];
    UIColor *colorBelow1 = [UIColor colorWithHue:[UIColor addDegrees:-15 toDegree:h]/360 saturation:(s-5)/100 brightness:(b-5)/100 alpha:a];
    UIColor *colorBelow2 = [UIColor colorWithHue:[UIColor addDegrees:-30 toDegree:h]/360 saturation:(s-5)/100 brightness:(b-10)/100 alpha:a];
    
    return @[colorAbove2,colorAbove1,color,colorBelow1,colorBelow2];
}

+ (NSArray *)monochromaticColorsFromHue:(float)h saturation:(float)s brightness:(float)b alpha:(float)a
{
    UIColor *color = [UIColor colorWithHue:h/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorAbove1 = [UIColor colorWithHue:h/360 saturation:s/100 brightness:(b/2)/100 alpha:a];
    UIColor *colorAbove2 = [UIColor colorWithHue:h/360 saturation:(s/2)/100 brightness:(b/3)/100 alpha:a];
    UIColor *colorBelow1 = [UIColor colorWithHue:h/360 saturation:(s/3)/100 brightness:(2*b/3)/100 alpha:a];
    UIColor *colorBelow2 = [UIColor colorWithHue:h/360 saturation:s/100 brightness:(4*b/5)/100 alpha:a];
    
    return @[colorAbove2,colorAbove1,color,colorBelow1,colorBelow2];
}

+ (NSArray *)triadColorsFromHue:(float)h saturation:(float)s brightness:(float)b alpha:(float)a
{
    UIColor *color = [UIColor colorWithHue:h/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorAbove1 = [UIColor colorWithHue:[UIColor addDegrees:120 toDegree:h]/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorAbove2 = [UIColor colorWithHue:[UIColor addDegrees:120 toDegree:h]/360 saturation:(7*s/6)/100 brightness:(b-5)/100 alpha:a];
    UIColor *colorBelow1 = [UIColor colorWithHue:[UIColor addDegrees:240 toDegree:h]/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorBelow2 = [UIColor colorWithHue:[UIColor addDegrees:240 toDegree:h]/360 saturation:(7*s/6)/100 brightness:(b-5)/100 alpha:a];
    
    return @[colorAbove2,colorAbove1,color,colorBelow1,colorBelow2];
}

+ (NSArray *)complementaryColorsFromHue:(float)h saturation:(float)s brightness:(float)b alpha:(float)a
{
    UIColor *color = [UIColor colorWithHue:h/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorAbove1 = [UIColor colorWithHue:h/360 saturation:(5*s/7)/100 brightness:b/100 alpha:a];
    UIColor *colorAbove2 = [UIColor colorWithHue:h/360 saturation:s/100 brightness:(4*b/5)/100 alpha:a];
    UIColor *colorBelow1 = [UIColor colorWithHue:[UIColor addDegrees:180 toDegree:h]/360 saturation:s/100 brightness:b/100 alpha:a];
    UIColor *colorBelow2 = [UIColor colorWithHue:[UIColor addDegrees:180 toDegree:h]/360 saturation:(5*s/7)/100 brightness:b/100 alpha:a];
    
    return @[colorAbove2,colorAbove1,color,colorBelow1,colorBelow2];
}

+ (float)addDegrees:(float)addDeg toDegree:(float)staticDeg
{
    staticDeg += addDeg;
    if (staticDeg > 360) {
        float offset = staticDeg - 360;
        return offset;
    }
    else if (staticDeg < 0) {
        return -1 * staticDeg;
    }
    else {
        return staticDeg;
    }
}

#pragma mark - iOS 7 Colors

+ (instancetype)red7Color;
{
    return [UIColor colorWithRed:1.0f green:0.22f blue:0.22f alpha:1.0f];
}

+ (instancetype)orange7Color;
{
    return [UIColor colorWithRed:1.0f green:0.58f blue:0.21f alpha:1.0f];
}

+ (instancetype)yellow7Color;
{
    return [UIColor colorWithRed:1.0f green:0.79f blue:0.28f alpha:1.0f];
}

+ (instancetype)green7Color;
{
    return [UIColor colorWithRed:0.27f green:0.85f blue:0.46f alpha:1.0f];
}

+ (instancetype)lightBlue7Color;
{
    return [UIColor colorWithRed:0.18f green:0.67f blue:0.84f alpha:1.0f];
}

+ (instancetype)darkBlue7Color;
{
    return [UIColor colorWithRed:0.0f green:0.49f blue:0.96f alpha:1.0f];
}

+ (instancetype)purple7Color;
{
    return [UIColor colorWithRed:0.35f green:0.35f blue:0.81f alpha:1.0f];
}

+ (instancetype)pink7Color;
{
    return [UIColor colorWithRed:1.0f green:0.17f blue:0.34f alpha:1.0f];
}

+ (instancetype)darkGray7Color;
{
    return [UIColor colorWithRed:0.56f green:0.56f blue:0.58f alpha:1.0f];
}

+ (instancetype)lightGray7Color;
{
    return [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0f];
}

#pragma mark - iOS 7 Gradient Colors

+ (instancetype)red7GradientStartColor;
{
    return [UIColor colorWithRed:1.0f green:0.37f blue:0.23f alpha:1.0f];
}

+ (instancetype)red7GradientEndColor;
{
    return [UIColor colorWithRed:1.0f green:0.16f blue:0.41f alpha:1.0f];
}

+ (instancetype)orange7GradientStartColor;
{
    return [UIColor colorWithRed:1.0f green:0.58f blue:0.0f alpha:1.0f];
}

+ (instancetype)orange7GradientEndColor;
{
    return [UIColor colorWithRed:1.0f green:0.37f blue:0.23f alpha:1.0f];
}

+ (instancetype)yellow7GradientStartColor;
{
    return [UIColor colorWithRed:1.0f green:0.86f blue:0.3f alpha:1.0f];
}

+ (instancetype)yellow7GradientEndColor;
{
    return [UIColor colorWithRed:1.0f green:0.8f blue:0.01f alpha:1.0f];
}

+ (instancetype)green7GradientStartColor;
{
    return [UIColor colorWithRed:0.53f green:0.99f blue:0.44f alpha:1.0f];
}

+ (instancetype)green7GradientEndColor;
{
    return [UIColor colorWithRed:0.04f green:0.83f blue:0.09f alpha:1.0f];
}

+ (instancetype)teal7GradientStartColor;
{
    return [UIColor colorWithRed:0.32f green:0.93f blue:0.78f alpha:1.0f];
}

+ (instancetype)teal7GradientEndColor;
{
    return [UIColor colorWithRed:0.35f green:0.78f blue:0.98f alpha:1.0f];
}

+ (instancetype)blue7GradientStartColor;
{
    return [UIColor colorWithRed:0.10f green:0.84f blue:0.99f alpha:1.0f];
}

+ (instancetype)blue7GradientEndColor;
{
    return [UIColor colorWithRed:0.11f green:0.38f blue:0.94f alpha:1.0f];
}

+ (instancetype)violet7GradientStartColor;
{
    return [UIColor colorWithRed:0.78f green:0.27f blue:0.99f alpha:1.0f];
}

+ (instancetype)violet7GradientEndColor;
{
    return [UIColor colorWithRed:0.35f green:0.34f blue:0.84f alpha:1.0f];
}

+ (instancetype)magenta7GradientStartColor;
{
    return [UIColor colorWithRed:0.94f green:0.30f blue:0.71f alpha:1.0f];
}

+ (instancetype)magenta7GradientEndColor;
{
    return [UIColor colorWithRed:0.78f green:0.26f blue:0.99f alpha:1.0f];
}

+ (instancetype)black7GradientStartColor;
{
    return [UIColor colorWithRed:0.29f green:0.29f blue:0.29f alpha:1.0f];
}

+ (instancetype)black7GradientEndColor;
{
    return [UIColor colorWithRed:0.17f green:0.17f blue:0.17f alpha:1.0f];
}

+ (instancetype)silver7GradientStartColor;
{
    return [UIColor colorWithRed:0.86f green:0.87f blue:0.87f alpha:1.0f];
}

+ (instancetype)silver7GradientEndColor;
{
    return [UIColor colorWithRed:0.54f green:0.55f blue:0.56f alpha:1.0f];
}

@end
