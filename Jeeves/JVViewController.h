//
//  JVViewController.h
//  Kobi
//
//  Created by Simon on 18/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVViewController : UIViewController

- (void)refreshView;

@end
