//
//  JVDeviceClass.h
//  Kobi
//
//  Created by Simon on 17/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVData.h"

typedef NS_ENUM(char, DeviceClassConflictTypes)
{
    DeviceClassConflictType_NAME = 0,
    DeviceClassConflictType_ICON
};

@interface JVDeviceClassData : JVData

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) UIImage* icon;

- (id)initWithName:(NSString*)name andIcon:(UIImage*)icon;

@end
