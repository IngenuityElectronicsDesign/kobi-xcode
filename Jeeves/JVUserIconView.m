//
//  JVUserIconView.m
//  Jeeves
//
//  Created by Simon on 4/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVUserIconView.h"

@implementation JVUserIconView

- (id)initWithFrame:(CGRect)frame userData:(JVUserData*)userData andIndex:(int)index
{
    if( self = [super initWithFrame:frame andIndex:index] )
    {
        [self setWithUserData:userData];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame blankAsAdmin:(BOOL)asAdmin andIndex:(int)index
{
    if( self = [super initWithFrame:frame andIndex:index] )
    {
        [self setBlankAsAdmin:asAdmin];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame asBlankWithIndex:(int)index
{
    if( self = [super initWithFrame:frame andIndex:index] )
    {
        [self setBlank];
    }
    return self;
}

- (void)setWithUserData:(JVUserData *)userData
{
    _userData = userData;
    
    [[super button] setUserInteractionEnabled:YES];
    [[super button] setImage:[_userData portrait] forState:UIControlStateNormal];
    [[[super button] imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[_userData tintColorIndex]]];
    
    [[super label] setText:[_userData name]];
}

- (void)setBlankAsAdmin:(BOOL)asAdmin
{
    _userData = nil;
    
    [[super button] setUserInteractionEnabled:YES];
    [[super button] setImage:[[UIImage imageNamed:(asAdmin ? @"Admin.png" : @"Children.png")] imageWithTintColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] loggedInUser] tintColorIndex]]] forState:UIControlStateNormal];
    
    [[[super button] imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[_userData tintColorIndex]]];
    
    [[super label] setText:(asAdmin ? @"Add Admin" : @"Add Child")];
}

- (void)setBlank
{
    _userData = nil;
    
    [[super button] setUserInteractionEnabled:NO];
    [[super button] setImage:nil forState:UIControlStateNormal];
    [[super label] setText:@""];
}

@end
