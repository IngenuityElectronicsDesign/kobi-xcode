//
//  JVEditUserTableViewController.m
//  Kobi
//
//  Created by Simon on 9/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVEditUserTableViewController.h"
#import "JVPinViewController.h"

#define kMaxNameLength 12

@interface JVEditUserTableViewController ()

@end

@implementation JVEditUserTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    if( self = [super initWithStyle:style] )
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if( _selectedUser != nil )
    {
        [self setTitle:[NSString stringWithFormat:@"Edit %@", [_selectedUser name]]];
        [_nameTextField setText:[_selectedUser name]];
        [_portraitButton setImage:[_selectedUser portrait] forState:UIControlStateNormal];
        [[_portraitButton imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[_selectedUser tintColorIndex]]];
        [_removeLabel setText:( _isAdmin ? @"Remove Admin" : @"Remove Child" )];
        
        _selectedIndexPath = [NSIndexPath indexPathForRow:[_selectedUser tintColorIndex]-1 inSection:2];
        [[self tableView] reloadData];
        
        _modifiedUser = [[JVUserData alloc] initWithName:[_selectedUser name] portrait:[_selectedUser portrait] colorIndex:[_selectedUser tintColorIndex] andIsAdmin:[_selectedUser isAdmin]];
        [_modifiedUser setPinCode:[[JVPinCodeData alloc] initWithPinString:[[_selectedUser pinCode] getPinAsString]]];
        
        _pinIsShowing = NO;
    }
    else
    {
        [self setTitle:( _isAdmin ? @"Add Admin" : @"Add Child" ) ];
        [_nameTextField setText:( _isAdmin ? @"Admin" : @"Child" ) ];
        [_portraitButton setImage:( _isAdmin ? [UIImage imageNamed:@"Admin.png"] : [UIImage imageNamed:@"Children.png"] ) forState:UIControlStateNormal];
        [[_portraitButton imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:SYSTEM]];
        
        _modifiedUser = [[JVUserData alloc] initWithName:@"" portrait:nil colorIndex:SYSTEM andIsAdmin:_isAdmin];
        [_modifiedUser setPinCode:nil];
        
        _selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:2];
        
        _pinIsShowing = YES;
    }
    
    if( _selectedUser == [[JVShared s] loggedInUser] )
    {
        [self setTitle:@"Edit My Info"];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if( _pinIsShowing )
    {
        [[_pinCell textLabel] setText:(_selectedUser != nil ? @"Change PIN" : @"Set PIN")];
        [[_pinCell detailTextLabel] setText:([_modifiedUser pinCode] != nil ?[[_modifiedUser pinCode] getPinAsString] : @"- - - -")];
        [_pinCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [self compareUsers];
    }
    
    [_nameTextField setDelegate:self];
    [_nameTextField resignFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_nameTextField setDelegate:nil];
    [_inputErrorAlertView dismissWithClickedButtonIndex:0 animated:NO];
    [_imagePickerActionSheet dismissWithClickedButtonIndex:0 animated:NO];
    [_removeUserActionSheet dismissWithClickedButtonIndex:0 animated:NO];
}

- (void)didReceiveMemoryWarning
{
    _modifiedUser = nil;
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( section == 0 )
    {
        return 1;
    }
    else if( section == 1 )
    {
        return 1;
    }
    else if( section == 2 )
    {
        if( _isAdmin )
        {
            return 0;
        }
        else
        {
            return 7;
        }
    }
    else if( section == 3 )
    {
        if( _selectedUser != nil && _selectedUser != [[JVShared s] loggedInUser] )
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    return 0;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if( section == 2 && !_isAdmin )
    {
        return [NSString stringWithFormat:@"Pick %@'s Faviourite Colour", (_selectedUser != nil ? [_selectedUser name] : @"Child")];
    }
    return @"";
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if( indexPath.section == 2 )
    {
        if( indexPath.row == _selectedIndexPath.row )
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if( indexPath.section == 1 )
    {
        if( !_pinIsShowing )
        {
            _pinIsShowing = YES;
            
            [[_pinCell textLabel] setText:@"Change PIN"];
            [[_pinCell detailTextLabel] setText:[[_modifiedUser pinCode] getPinAsString]];
            [_pinCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        else
        {
            JVPinViewController* toPush = [[[JVShared s] storyboard] instantiateViewControllerWithIdentifier:@"PinVC"];
            if( [_modifiedUser pinCode] == nil )
            {
                [_modifiedUser setPinCode:[[JVPinCodeData alloc] init]];
            }
            [toPush passPinCode:[_modifiedUser pinCode]];
            [[self navigationController] pushViewController:toPush animated:YES];
        }
    }
    
    if( indexPath.section == 2 )
    {
        _selectedIndexPath = indexPath;
        [_modifiedUser setTintColorIndex:(UserTintColorIndex)(indexPath.row + 1)];
        if( [[JVShared s] isUser:_modifiedUser conflictedWithType:CONFLICT_COLOR withExceptedUser:_selectedUser] )
        {
            [_modifiedUser setTintColorIndex:[_selectedUser tintColorIndex]];
            _inputErrorAlertView = [[UIAlertView alloc] initWithTitle:@"Colour Already Taken" message:@"Please pick another colour." delegate:self cancelButtonTitle:@"Got it!" otherButtonTitles:nil];
            [_inputErrorAlertView show];
            _selectedIndexPath = [NSIndexPath indexPathForRow:[_selectedUser tintColorIndex]-1 inSection:2];
        }
        
        [[self tableView] reloadData];
        [self compareUsers];
    }
    
    if( indexPath.section == 3 )
    {
        _removeUserActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure you want to remove %@?\n\nThis will permanatly delete this account, its data, and its history. This cannot be undone.", [_selectedUser name]] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:[NSString stringWithFormat:@"Remove %@", [_selectedUser name]] otherButtonTitles:nil];
        [_removeUserActionSheet showInView:[self view]];
    }
}

#pragma mark - IBActions

- (IBAction)nameTextFieldEditingDidEnd:(UITextField *)sender
{
    NSString* newName = [sender text];
    
    if( [newName isEqualToString:@""] || [newName length] <= 0 )
    {
        [sender setText:[_modifiedUser name]];
        return;
    }
    if( [newName length] > kMaxNameLength )
    {
        NSRange stringRange = {0, MIN([[sender text] length], kMaxNameLength)};
        stringRange = [[sender text] rangeOfComposedCharacterSequencesForRange:stringRange];
        [sender setText:[[sender text] substringWithRange:stringRange]];
        newName = [sender text];
    }
    
    [_modifiedUser setName:newName];
    
    if( [[JVShared s] isUser:_modifiedUser conflictedWithType:CONFLICT_NAME withExceptedUser:_selectedUser] )
    {
        [sender setText:(_selectedUser != nil ? [_selectedUser name] : _isAdmin ? @"Admin" : @"Child" )];
        [_modifiedUser setName:(_selectedUser != nil ? [_selectedUser name] : _isAdmin ? @"Admin" : @"Child" )];
        _inputErrorAlertView = [[UIAlertView alloc] initWithTitle:@"Name Already Taken" message:@"Please enter a unique name." delegate:self cancelButtonTitle:@"Got it!" otherButtonTitles:nil];
        [_inputErrorAlertView show];
    }
    
    [self compareUsers];
}

- (IBAction)portraitButtonPressed:(id)sender
{
    [self willDisplayImagePicker];
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - AlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [alertView setDelegate:nil];
    alertView = nil;
}

#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( actionSheet == _imagePickerActionSheet )
    {
        if( buttonIndex == 0 )
        {
            [self displayImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        else if( buttonIndex == 1 )
        {
            [self displayImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
    }
    if( actionSheet == _removeUserActionSheet )
    {
        if( buttonIndex == 0 )
        {
            [[JVShared s] removeUserWithUserData:_selectedUser fromAdmin:[_selectedUser isAdmin]];
            [self discardButtonPressed];
        }
    }
    
    [actionSheet setDelegate:nil];
    actionSheet = nil;
}

#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    CGFloat width = originalImage.size.width*0.2f;
    CGFloat height = originalImage.size.height*0.2f;
    
    CGSize size = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(size);
    [originalImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    NSLog(@"w: %f h: %f", width, height);
    
    if( width != height )
    {
        CGFloat length = MIN(width, height);
        CGFloat widthOffset = (width - length) / 2;
        CGFloat heightOffset = (height - length) / 2;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(length, length), NO, 0.);
        [image drawAtPoint:CGPointMake(-widthOffset, -heightOffset) blendMode:kCGBlendModeCopy alpha:1.];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    [_modifiedUser setPortrait:image];
    [_portraitButton setImage:image forState:UIControlStateNormal];
    [_portraitButton setImage:image forState:UIControlStateSelected];
    [_portraitButton setImage:image forState:UIControlStateHighlighted];
    [[self tableView] reloadData];
    
    [[_portraitButton imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[_modifiedUser tintColorIndex]]];
    [self compareUsers];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    picker = nil;
}

- (void)willDisplayImagePicker
{
    if( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
    {
        _imagePickerActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take New Photo", @"Choose From Library", nil];
        [_imagePickerActionSheet showInView:[self view]];
    }
    else
    {
        [self displayImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

- (void)displayImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    _imagePickerController = [[UIImagePickerController alloc] init];
    [_imagePickerController setSourceType:sourceType];
    [_imagePickerController setDelegate:self];
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

#pragma mark - Parent Methods

- (void)saveButtonPressed
{
    if( _selectedUser != nil )
    {
        [_selectedUser setName:[_modifiedUser name]];
        [_selectedUser setPortrait:[_modifiedUser portrait]];
        [_selectedUser setTintColorIndex:[_modifiedUser tintColorIndex]];
        [_selectedUser setPinCode:[[JVPinCodeData alloc] initWithPinString:[[_modifiedUser pinCode] getPinAsString]]];
        
        [self setTitle:[NSString stringWithFormat:@"Edit %@", [_selectedUser name]]];
        [[_portraitButton imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[_modifiedUser tintColorIndex]]];
        [[self tableView] reloadData];
    }
    else
    {
        [[JVShared s] addUserWithUserData:_modifiedUser];
        [self discardButtonPressed];
    }
    
    [self compareUsers];
}

- (void)discardButtonPressed
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)compareUsers
{
    if( _selectedUser != nil )
    {
        [super dataModified:![_selectedUser isEqualTo:_modifiedUser]];
    }
    else
    {
        if( ![[_modifiedUser name] isEqualToString:@""] && [_modifiedUser portrait] != nil && (_isAdmin ? [_modifiedUser tintColorIndex] == SYSTEM : [_modifiedUser tintColorIndex] != SYSTEM) && [_modifiedUser pinCode] != nil )
        {
            [super dataModified:YES];
        }
        else
        {
            [super dataModified:NO];
        }
    }
}

@end
