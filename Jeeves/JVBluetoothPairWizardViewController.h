//
//  JVBluetoothPairWizardViewController.h
//  Jeeves
//
//  Created by Simon on 21/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYBlurIntroductionView.h"
#import "MYIntroductionPanel.h"
#import "AMBlurView.h"

@interface JVBluetoothPairWizardViewController : UIViewController <MYIntroductionDelegate>

@end
