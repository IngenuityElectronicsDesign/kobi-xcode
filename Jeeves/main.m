//
//  main.m
//  Jeeves
//
//  Created by Simon on 10/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JVAppDelegate class]));
    }
}
