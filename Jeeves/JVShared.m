//
//  JVShared.m
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVShared.h"

@implementation JVShared

+ (instancetype)s
{
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,
    ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id)init
{
    if( self = [super init] )
    {
        // System
        _widescreen = NO;
        if( [[UIScreen mainScreen] bounds].size.height == 568 )
        {
            _widescreen = YES;
        }
        
        _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        // Color
        _systemTintColor = [UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1];
        
        _userTintColorsArray = @[_systemTintColor, [UIColor red7Color], [UIColor orange7Color], [UIColor yellow7Color], [UIColor green7Color], [UIColor lightBlue7Color], [UIColor purple7Color], [UIColor pink7Color]];
        
        // Users
        _adminUsersArray = [[NSMutableArray alloc] init];
        
        [self addUserWithName:@"Dad" portrait:[UIImage imageNamed:@"Dad.png"] colorIndex:SYSTEM andIsAdmin:YES];
        [_adminUsersArray[0] setPinCodeWithString:@"1111"];
        
        _childUsersArray = [[NSMutableArray alloc] init];
        
        NSArray* childNames = @[@"Johnny", @"Sally", @"Billy"];
        NSArray* portraits = @[[UIImage imageNamed:@"Johnny.png"], [UIImage imageNamed:@"Sally.png"], [UIImage imageNamed:@"Billy.png"], [UIImage imageNamed:@"Johnny.png"], [UIImage imageNamed:@"Sally.png"]];
        NSArray* colors = @[@(ORANGE), @(PURPLE), @(RED), @(GREEN), @(PINK)];
        
        for( int i = 0; i < [childNames count]; i++ )
        {
            UIImage* portrait = portraits[i];
            
            [self addUserWithName:childNames[i] portrait:portrait colorIndex:(UserTintColorIndex)[colors[i] intValue] andIsAdmin:NO];
        }
        
        [_childUsersArray[0] setPinCodeWithString:@"2222"];
        [_childUsersArray[1] setPinCodeWithString:@"3333"];
        [_childUsersArray[2] setPinCodeWithString:@"4444"];
        
        _loggedInUser = nil;
        _selectedUsersIndexSet = [[NSMutableIndexSet alloc] init];
        [_selectedUsersIndexSet addIndex:0];
        
        // Device Classes
        _deviceClassDataContainer = [[JVDeviceClassDataContainer alloc] init];
        _selectedDeviceClassesIndexSet = [[NSMutableIndexSet alloc] init];
        [_selectedDeviceClassesIndexSet addIndex:0];
        
        NSArray* deviceNames = @[@"TV", @"Games", @"Phone", @"Laptop"];
        NSArray* deviceIcons = @[[UIImage imageNamed:@"TV.png"], [UIImage imageNamed:@"Controller.png"], [UIImage imageNamed:@"Speakers.png"], [UIImage imageNamed:@"Laptop.png"]];
        
        for( int i = 0; i < [deviceNames count]; i++ )
        {
            [self addDeviceClassesWithName:deviceNames[i] andIcon:deviceIcons[i]];
        }
        
        // Kobi Boxes
        _kobiBoxesArray = [[NSMutableArray alloc] init];
        
        // Kobi Settings
        _kobiGlobalSettings = [[JVKobiGlobalData alloc] init];
    }
    return self;
}

- (void)setupDataStructures
{
    // Kobi Data
    _kobiQuotaDataContainer = [[JVQuotaDataContainer alloc] init];
    
    JVQuotaDataContainer* otherContainer = [[JVQuotaDataContainer alloc] init];
    
    JVQuotaData* data = [[JVQuotaData alloc] init];
    [data setWeeklyQuota:12];
    
    //[otherContainer dataArray][0][1] = data;
    //[otherContainer dataArray][0][2] = data;
    
    NSMutableSet* conflictsArray = [[NSMutableSet alloc] init];
    
    NSLog(@"isEqual: %d || conflicts: %@", [_kobiQuotaDataContainer isEqualToOther:otherContainer  withConflictsArray:conflictsArray], conflictsArray);
    
    /*if( [_dataArray count] > 0 )
     {
     NSMutableArray* conflictsArray = [[NSMutableArray alloc] init];
     BOOL isEqual = [data isEqualToOther:_dataArray[0] withConflictsArray:conflictsArray];
     
     NSLog(@"isEqual: %d || conflicts: %@", isEqual, conflictsArray);
     }*/
}

- (void)syncWithJeevesNetwork
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kSharedDataNeedsRefresh object:nil];
}

#pragma mark - User Retrieval

- (JVUserData*)userDataForIndex:(int)index
{
    if( index >= [_adminUsersArray count] + [_childUsersArray count] )
    {
        NSLog(@"Error getting user: Index %d out of range", index);
        return nil;
    }
    
    if( index < [_adminUsersArray count] )
    {
        return _adminUsersArray[index];
    }
    else
    {
        return _childUsersArray[index-[_adminUsersArray count]];
    }
}

- (id)userDataForIndex:(int)index fromAdmin:(BOOL)fromAdmin
{
    if( fromAdmin && index < [_adminUsersArray count] )
    {
        return _adminUsersArray[index];
    }
    else if( !fromAdmin && index < [_childUsersArray count] )
    {
        return _childUsersArray[index];
    }
    return nil;
}

#pragma mark - User Modification

- (BOOL)addDeviceClassesWithName:(NSString*)name andIcon:(UIImage*)icon
{
    return [_deviceClassDataContainer addDeviceClassWithName:name andIcon:icon];
}

- (BOOL)addUserWithName:(NSString*)name portrait:(UIImage*)portrait colorIndex:(UserTintColorIndex)index andIsAdmin:(BOOL)isAdmin
{
    if( ( isAdmin && [_adminUsersArray count] >= kAdminUsersArrayMaxSize ) || ( !isAdmin && [_childUsersArray count] >= kChildUsersArrayMaxSize ) )
    {
        NSLog(@"Error adding user: Max number of users already reached.");
        return NO;
    }
    
    if( isAdmin )
    {
        [_adminUsersArray addObject:[[JVUserData alloc] initWithName:name portrait:portrait andColorIndex:index]];
    }
    else
    {
        [_childUsersArray addObject:[[JVUserData alloc] initWithName:name portrait:portrait colorIndex:index andIsAdmin:NO]];
    }
    return YES;
}

- (BOOL)addUserWithUserData:(JVUserData*)newUser
{
    if( ( [newUser isAdmin] && [_adminUsersArray count] == kAdminUsersArrayMaxSize ) || ( ![newUser isAdmin] && [_childUsersArray count] == kChildUsersArrayMaxSize ) )
    {
        NSLog(@"Error adding user: Max number of users already reached.");
        return NO;
    }
    
    if( [newUser isAdmin] )
    {
        [_adminUsersArray addObject:newUser];
    }
    else
    {
        [_childUsersArray addObject:newUser];
    }
    return YES;
}

- (void)removeUserWithUserData:(JVUserData*)userData fromAdmin:(BOOL)fromAdmin
{
    if( fromAdmin )
    {
        [_adminUsersArray removeObject:userData];
    }
    else
    {
        [_childUsersArray removeObject:userData];
    }
}

#pragma mark - User Checking

- (BOOL)isUser:(JVUserData *)user conflictedWithType:(ConflictType)type withExceptedUser:(JVUserData *)exceptedUser
{
    for( int i = 0; i < [self totalUserCount]; i++ )
    {
        if( [self userDataForIndex:i] == exceptedUser )
        {
            continue;
        }
        if( [user isConflicted:[self userDataForIndex:i] withType:type] )
        {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isPinConflicted:(JVPinCodeData*)otherPin
{
    for( int i = 0; i < [self totalUserCount]; i++ )
    {
        if( [[self userDataForIndex:i] isPinConflicted:otherPin] )
        {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Tint Color Retrieval

- (UIColor*)userTintColorForIndex:(UserTintColorIndex)index
{
    return _userTintColorsArray[(char)index];
}

#pragma mark - Getters and Setters

- (int)totalUserCount
{
    return (int)([_adminUsersArray count] + [_childUsersArray count]);
}

@end
