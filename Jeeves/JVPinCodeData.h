//
//  JVPinCode.h
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVData.h"

typedef NS_ENUM(char, PinCodeConflictTypes)
{
    PinCodeConflictType_PIN = 0
};

@interface JVPinCodeData : JVData

@property (nonatomic) unsigned char pin;

- (id)initWithPinString:(NSString*)pinString;

- (BOOL)isEqualToPinCode:(JVPinCodeData*)aPinCode;
- (NSString*)getPinAsString;
- (void)setPinWithString:(NSString*)pinString;

@end
