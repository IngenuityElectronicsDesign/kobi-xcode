//
//  JVManageDeviceClassTableViewController.h
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVDataSyncTableViewController.h"
#import "JVDeviceClassCell.h"

@interface JVManageDeviceClassTableViewController : JVDataSyncTableViewController <UIActionSheetDelegate, JVDeviceClassCellDelegate>

@property (nonatomic, strong) UIActionSheet* deleteConfirmationActionSheet;
@property (nonatomic) NSUInteger deletionIndex;

@end
