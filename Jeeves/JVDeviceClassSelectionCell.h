//
//  JVDeviceClassSelectionCell.h
//  Kobi
//
//  Created by Simon on 8/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XIImage.h"
#import "MultiSelectSegmentedControl.h"

@protocol JVDeviceClassSelectionCellDelegate <NSObject>

- (void)deviceClassSelectionDidChange;

@end

@interface JVDeviceClassSelectionCell : UITableViewCell <MultiSelectSegmentedControlDelegate>

@property (nonatomic, weak) id<JVDeviceClassSelectionCellDelegate> delegate;

@end
