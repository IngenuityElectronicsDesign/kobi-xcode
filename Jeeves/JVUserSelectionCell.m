//
//  JVUserSelectionCell.m
//  Kobi
//
//  Created by Simon on 17/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVUserSelectionCell.h"

@interface JVUserSelectionCell ()

@property (nonatomic, strong) MultiSelectSegmentedControl* segmentedControl;
//@property (nonatomic, strong) UISegmentedControl* allButton;

@end

@implementation JVUserSelectionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if( self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] )
    {
        _segmentedControl = [[MultiSelectSegmentedControl alloc] initWithFrame:CGRectMake(20, 8, 280, 29)];
        
        for( int i = 0; i < [[[JVShared s] childUsersArray] count]; i++ )
        {
            JVUserData* user = [[JVShared s] childUsersArray][i];
            
            [_segmentedControl insertSegmentWithTitle:[user name] atIndex:i animated:NO];
        }
        
        [_segmentedControl setSelectedSegmentIndexes:[[JVShared s] selectedUsersIndexSet]];
        [_segmentedControl setTintColor:[[JVShared s] systemTintColor]];
        [_segmentedControl setDelegate:self];
        [self addSubview:_segmentedControl];
        
        /*
        _allButton = [[UISegmentedControl alloc] initWithFrame:CGRectMake(271, 8, 29, 29)];
        [_allButton setTintColor:[[JVShared s] systemTintColor]];
        [_allButton setMomentary:YES];
        [_allButton addTarget:self action:@selector(allButtonPressed) forControlEvents:UIControlEventValueChanged];
        [_allButton insertSegmentWithTitle:@"X" atIndex:0 animated:NO];
        [self addSubview:_allButton];*/
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:NO];
}

/*- (void)allButtonPressed
{
    [_segmentedControl selectAllSegments:YES];
}*/

#pragma mark - TabView Delegate

- (void)multiSelect:(MultiSelectSegmentedControl *)multiSelecSegmendedControl didChangeValue:(BOOL)value atIndex:(NSUInteger)index
{
    
}

- (void)multiSelect:(MultiSelectSegmentedControl *)multiSelectSegmentedControl indexSetDidChange:(NSIndexSet *)indexSet
{
    [[[JVShared s] selectedUsersIndexSet] removeAllIndexes];
    [[[JVShared s] selectedUsersIndexSet] addIndexes:indexSet];
    [[self delegate] userSelectionDidChange];
}

@end
