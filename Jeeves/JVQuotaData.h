//
//  JVQuotaData.h
//  Kobi
//
//  Created by Simon on 13/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVData.h"

typedef NS_ENUM(char, QuotaDataConflictTypes)
{
    QuotaDataConflictTypes_MODE = 0,
    QuotaDataConflictTypes_WEEKLY_QUOTA,
    QuotaDataConflictTypes_DAILY_QUOTA,
    QuotaDataConflictTypes_EMERGENCY_ENABLED,
    QuotaDataConflictTypes_EMERGENCY_QUOTA
};

typedef NS_ENUM(char, KobiMode)
{
    KOBI_MODE_CONFLICT = -1,
    MONITOR_MODE,
    QUOTA_MODE,
    SUSPEND_MODE
};

@interface JVQuotaData : JVData

@property (nonatomic) KobiMode mode;
@property (nonatomic) unsigned char weeklyQuota;
@property (nonatomic) NSMutableArray* dailyQuotas;
@property (nonatomic) BOOL emergencyHoursEnabled;
@property (nonatomic) unsigned char emergencyHourQuota;

- (void)setDailyQuotasWithArray:(NSArray*)dailyQuotas;

@end
