//
//  JVBluetoothPairWizardViewController.m
//  Jeeves
//
//  Created by Simon on 21/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVBluetoothPairWizardViewController.h"

@interface JVBluetoothPairWizardViewController ()

@property (nonatomic, strong) MYBlurIntroductionView* introView;

@end

@implementation JVBluetoothPairWizardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self buildIntro];
}

- (void)buildIntro
{
    MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Setup Jeeves" description:@"Plug Jeeves into an outlet where you would like to use it. Switch Jeeves on, and wait until the light is green.\n\nSwipe left to continue."];
    
    MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Launch the Settings App" description:@"Launch the Settings app from your iPhone Home Menu. You will need to leave the Jeeves app to do this." image:[UIImage imageNamed:@"BluetoothWizard1.png"]];
    
    MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Open Bluetooth Settings" description:@"In the Settings app, tap the Bluetooth icon." image:[UIImage imageNamed:@"BluetoothWizard2.png"]];
    
    MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Turn Bluetooth On" description:@"Slide the Bluetooth slider to the on position." image:[UIImage imageNamed:@"BluetoothWizard3.png"]];
    
    MYIntroductionPanel *panel5 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Pair to Jeeves" description:@"In the device list, tap Jeeves - Not Connected. This will initiate the bluetooth pairing." image:[UIImage imageNamed:@"BluetoothWizard4.png"]];
    
    MYIntroductionPanel *panel6 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"That's it!" description:@"The Jeeves should now be connected.\n\nSwipe left to close." image:[UIImage imageNamed:@"BluetoothWizard5.png"]];
    
    NSArray *panels = @[panel1, panel2, panel3, panel4, panel5, panel6];
    
    _introView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    _introView.delegate = self;
    //_introView.BackgroundImageView.Image
    
    [_introView buildIntroductionWithPanels:panels];
    
    [_introView setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1]];
    
    [[_introView RightSkipButton] setHidden:YES];
    [[_introView PageControl] setEnabled:NO];
    //[[_introView PageControl] setNumberOfPages:[[_introView PageControl] numberOfPages] + 1];
    [_introView setRightSkipButton:nil];
    
    [[self view] addSubview:_introView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MYIntroduction Delegate

-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex
{
    NSLog(@"Introduction did change to panel %ld", (long)panelIndex);
    
    //[_introView setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1]];
    
    if( panelIndex >= 3 )
    {
        [[_introView RightSkipButton] setHidden:NO];
    }
    else
    {
        [[_introView RightSkipButton] setHidden:YES];
    }
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end