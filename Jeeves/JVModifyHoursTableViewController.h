//
//  JVModifyHoursTableViewController.h
//  Jeeves
//
//  Created by Simon on 28/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVModifyHoursTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl *hoursTypeSegmentedControl;

@end
