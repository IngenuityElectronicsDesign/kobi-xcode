//
//  JVDataSyncTableViewController.h
//  Kobi
//
//  Created by Simon on 10/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVTableViewController.h"

@interface JVDataSyncTableViewController : JVTableViewController

- (void)dataModified:(BOOL)modified;

- (void)saveButtonPressed;
- (void)discardButtonPressed;

@end
