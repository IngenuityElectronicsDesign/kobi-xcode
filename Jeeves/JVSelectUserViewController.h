//
//  JVSelectUserViewController.h
//  Jeeves
//
//  Created by Simon on 16/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XIImageView.h"
#import "UIView+MTAnimation.h"
#import "JVViewController.h"
#import "JVPinView.h"
#import "JVUserIconView.h"

@interface JVSelectUserViewController : JVViewController <JVPinViewDelegate, JVIconViewDelegate>

@property (strong, nonatomic) NSMutableArray* userIcons;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

@property (nonatomic, strong) JVPinView* pinView;

@end
