//
//  JVLoggedInTableViewController.m
//  Jeeves
//
//  Created by Simon on 29/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVLoggedInTableViewController.h"
#import "KAProgressLabel.h"

@interface JVLoggedInTableViewController ()

@property (strong, nonatomic) IBOutlet KAProgressLabel *usageRingProgressView;

@property (strong, nonatomic) IBOutlet UILabel *changeStatusButtonLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;

@property (nonatomic) BOOL inUse;

@end

@implementation JVLoggedInTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIColor* tintColor = [UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1];
    
    [_usageRingProgressView setBorderWidth:12.0];
    [_usageRingProgressView setColorTable: @{ NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):tintColor, NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor darkGrayColor] }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_usageRingProgressView setProgress:9/12.f timing:TPPropertyAnimationTimingEaseOut duration:0.5 delay:0.25];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section == 2 && indexPath.row == 0 )
    {
        _inUse = !_inUse;
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    UIColor* tintColor = [UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1];
    
    if( _inUse )
    {
        [_changeStatusButtonLabel setText:@"Stop Using TV"];
        [_statusLabel setText:@"In Use for 0.5 Hours"];
        
        [[tableView cellForRowAtIndexPath:index] setBackgroundColor:tintColor];
        [[[tableView cellForRowAtIndexPath:index] textLabel] setTextColor:[UIColor whiteColor]];
    }
    else
    {
        [_changeStatusButtonLabel setText:@"Start Using TV"];
        [_statusLabel setText:@"Not In Use"];
        
        [[tableView cellForRowAtIndexPath:index] setBackgroundColor:[UIColor whiteColor]];
        [[[tableView cellForRowAtIndexPath:index] textLabel] setTextColor:[UIColor blackColor]];
    }
    
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
}

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
