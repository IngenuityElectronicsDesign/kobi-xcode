//
//  JVSelectUserViewController.m
//  Jeeves
//
//  Created by Simon on 16/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVSelectUserViewController.h"
#import "JVPinCodeData.h"

#define kTallPinScreenHeight 300
#define kShortPinScreenHeight 255

@interface JVSelectUserViewController ()

@property (nonatomic) int activeUserIndex;
@property (nonatomic) CGPoint activeUserOrigin;
@property (nonatomic) CGPoint inactiveUserOrigin;
@property (nonatomic) CGPoint offscreenUserOrigin;

@end

@implementation JVSelectUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if( self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil] )
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _activeUserIndex = -1;
    
    [_cancelButton setTitle:@""];
    
    _userIcons = [[NSMutableArray alloc] init];
    
    for( int i = 0; i < kTotalUsersArrayMaxSize; i++ )
    {
        JVUserIconView* userIcon = [[JVUserIconView alloc] initWithFrame:CGRectMake(0, 0, kIconWidth, kIconHeight) asBlankWithIndex:i];
        
        [userIcon setDelegate:self];
        [[self view] addSubview:userIcon];
        [_userIcons addObject:userIcon];
    }
    
    [self layoutUserIcons:NO];
    
    _activeUserOrigin = CGPointMake(112, ([[JVShared s] isWidescreen] ? 100 : 80));
    _inactiveUserOrigin = CGPointMake(112, 398);
    _offscreenUserOrigin = CGPointMake(112, 700);
    
    // Setup Pin View
    _pinView = [[JVPinView alloc] initWithFrame:CGRectMake(0, [[self view] frame].size.height, [[self view] frame].size.width, ([[JVShared s] isWidescreen] ? kTallPinScreenHeight : kShortPinScreenHeight)) isOverriding:NO];
    [_pinView setDelegate:self];
    [[self view] addSubview:_pinView];
    
    //[self performSegueWithIdentifier:@"bluetoothPairWizard" sender:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Set Navigation Bar to Defaults
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [[[self navigationController] navigationBar] setBarTintColor:nil];
    [[[self navigationController] navigationBar] setTintColor:[[JVShared s] systemTintColor]];
    [[[self navigationController] navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor],NSForegroundColorAttributeName, nil]];
}

- (void)refreshView
{
    [super refreshView];
    
    [self cancelButtonPressed:nil];
    
    for( int i = 0; i < kTotalUsersArrayMaxSize; i++ )
    {
        if( i < [[JVShared s] totalUserCount] )
        {
            [_userIcons[i] setWithUserData:[[JVShared s] userDataForIndex:i]];
        }
        else
        {
            [_userIcons[i] setBlank];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (void)userButtonPressedWithIndex:(int)index
{
    _activeUserIndex = index;
    
    for( int i = 0; i < [[JVShared s] totalUserCount]; i++ )
    {
        [self pinViewShouldBeHidden:NO];
        
        JVUserIconView* userIcon = _userIcons[i];
        
        if( i == index )
        {
            CGRect activeUserFrame = CGRectMake(_activeUserOrigin.x, _activeUserOrigin.y, [userIcon frame].size.width, [userIcon frame].size.height);
            
            [UIView mt_animateViews:@[userIcon] duration:0.25f timingFunction:kMTEaseOutQuad range:MTMakeAnimationRange(0, 0.7) options:0 animations:
            ^{
                [userIcon setFrame:activeUserFrame];
            }completion:
            ^{
                [UIView mt_animateViews:@[userIcon] duration:0.5f timingFunction:kMTEaseOutElastic range:MTAnimationRangeFull options:0 animations:
                 ^{
                     [userIcon setFrame:activeUserFrame];
                     [self setTitle:@"Enter PIN"];
                     [_cancelButton setTitle:@"Cancel"];
                 }completion:
                 ^{}];
            }];
        }
        else
        {
            CGRect unactiveUserFrame = CGRectMake(_inactiveUserOrigin.x, _inactiveUserOrigin.y, [userIcon frame].size.width, [userIcon frame].size.height);
            
            [UIView mt_animateViews:@[userIcon] duration:0.25f timingFunction:kMTEaseOutQuad range:MTMakeAnimationRange(0, 0.7) options:0 animations:
             ^{
                 [userIcon setFrame:unactiveUserFrame];
             }completion:
             ^{
                 [UIView mt_animateViews:@[userIcon] duration:0.25f timingFunction:kMTEaseOutElastic range:MTAnimationRangeFull options:0 animations:
                  ^{
                      [userIcon setFrame:unactiveUserFrame];
                  }completion:^{}];
             }];
            
            [UIView animateWithDuration:0.25f delay:0.0 options:(UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction) animations:
             ^{
                 [userIcon setAlpha:0.0f];
             } completion:
             ^(BOOL finished){ }
             ];
        }
    }
}

- (IBAction)cancelButtonPressed:(UIBarButtonItem *)sender
{
    [_cancelButton setTitle:@""];
    [self setTitle:@"Select User"];
    [_pinView resetPinDigits];
    [self pinViewShouldBeHidden:YES];
    
    for( int i = 0; i < [_userIcons count]; i++ )
    {
        if( i != _activeUserIndex )
        {
            [UIView animateWithDuration:0.5f delay:0.0 options:(UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction) animations:
             ^{
                 [(JVUserIconView*)_userIcons[i] setAlpha:1.0f];
             } completion:
             ^(BOOL finished){ }
             ];
        }
    }
    
    _activeUserIndex = -1;
    [self layoutUserIcons:YES];
}

#pragma mark - Layout Logic

- (void)layoutUserIcons:(BOOL)animated
{
    int x, y, vertSpacing;
    
    if( [[JVShared s] totalUserCount] > 4 )
    {
        vertSpacing = ( [[JVShared s] isWidescreen] ? 31 : 9 );
    }
    else if( [[JVShared s] totalUserCount] > 2 )
    {
        vertSpacing = ( [[JVShared s] isWidescreen] ? 84 : 55 );
    }
    else
    {
        vertSpacing = ( [[JVShared s] isWidescreen] ? 190 : 146 );
    }
    
    for( int i = 0; i < kTotalUsersArrayMaxSize; i++ )
    {
        if( i == _activeUserIndex )
        {
            continue;
        }
        
        JVUserIconView* userIcon = _userIcons[i];
        
        if( i >= [[JVShared s] totalUserCount] )
        {
            x = _offscreenUserOrigin.x;
            y = _offscreenUserOrigin.y;
        }
        else
        {
            if( i == [[JVShared s] totalUserCount]-1 && i % 2 == 0 )
            {
                x = 112;
            }
            else
            {
                x = kIconEdgePadding + ((kIconWidth+kIconHorizSpacing)*(i%2));
            }
            
            y = kTitleBarHeight + vertSpacing + ((kIconHeight+vertSpacing)*(i/2));
        }
        
        if( animated )
        {
            [UIView mt_animateViews:@[userIcon] duration:0.25f timingFunction:kMTEaseOutQuad range:MTMakeAnimationRange(0, 0.7) options:0 animations:
             ^{
                 [userIcon setFrame:CGRectMake(x, y, [userIcon frame].size.width, [userIcon frame].size.height)];
             }completion:
             ^{
                 [UIView mt_animateViews:@[userIcon] duration:0.5f timingFunction:kMTEaseOutElastic range:MTAnimationRangeFull options:0 animations:
                  ^{
                      [userIcon setFrame:CGRectMake(x, y, [userIcon frame].size.width, [userIcon frame].size.height)];
                  }completion:^{}];
             }];
        }
        else
        {
            [userIcon setFrame:CGRectMake(x, y, [userIcon frame].size.width, [userIcon frame].size.height)];
        }
        
        
    }
}

#pragma mark - Pin View

- (void)pinViewShouldBeHidden:(BOOL)hidden
{
    [UIView animateWithDuration:0.25f delay:0.0 options:(UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction) animations:
     ^{
         if( hidden )
         {
             [_pinView resetPinDigits];
             [_pinView setFrame:CGRectMake(0, [[self view] frame].size.height, [[self view] frame].size.width, ([[JVShared s] isWidescreen] ? kTallPinScreenHeight : kShortPinScreenHeight))];
         }
         else
         {
             [_pinView setTintColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] userDataForIndex:_activeUserIndex] tintColorIndex]]];
             [_cancelButton setTintColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] userDataForIndex:_activeUserIndex] tintColorIndex]]];
             [_pinView setTargetPinCode:[[[JVShared s] userDataForIndex:_activeUserIndex] pinCode]];
             [_pinView setFrame:CGRectMake(0, [[self view] frame].size.height-([[JVShared s] isWidescreen] ? kTallPinScreenHeight : kShortPinScreenHeight), [[self view] frame].size.width, ([[JVShared s] isWidescreen] ? kTallPinScreenHeight : kShortPinScreenHeight))];
         }
     } completion:
     ^(BOOL finished){ }
     ];
}

#pragma mark - PinView Delegate

- (void)didEnterPinCodeWithSuccess:(BOOL)success
{
    if( success )
    {
        [NSTimer scheduledTimerWithTimeInterval:0.75 target:self selector:@selector(enteredCorrectPIN) userInfo:nil repeats:NO];
        
        [_cancelButton setTitle:@""];
        
        [self setTitle:@"Correct PIN"];
    }
    else
    {
        [NSTimer scheduledTimerWithTimeInterval:0.75 target:self selector:@selector(enteredIncorrectPIN) userInfo:nil repeats:NO];
        
        [self setTitle:@"Incorrect PIN"];
    }
}

- (void)enteredCorrectPIN
{
    [self pinViewShouldBeHidden:YES];
    [[JVShared s] setLoggedInUser:[[JVShared s] userDataForIndex:_activeUserIndex]];
    
    UIViewController *toPush = nil;
    
    if( [[[JVShared s] loggedInUser] isAdmin] )
    {
        toPush = [[[JVShared s] storyboard] instantiateViewControllerWithIdentifier:@"ParentHomeView"];
    }
    else
    {
        toPush = [[[JVShared s] storyboard] instantiateViewControllerWithIdentifier:@"ChildHomeView"];
    }
    
    [[self navigationController] pushViewController:toPush animated:YES];
}

- (void)enteredIncorrectPIN
{
    [self setTitle:@"Enter PIN"];
    
    [_pinView resetPinDigits];
}

#pragma mark - UserIconView Delegate

- (void)didPressIconWithIndex:(int)index
{
    [self userButtonPressedWithIndex:index];
}

@end
