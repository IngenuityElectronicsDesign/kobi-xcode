//
//  JVUserDataContainer.h
//  Kobi
//
//  Created by Simon on 16/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVDataContainer.h"

#define kMaxNumAdminUsers 2
#define kMaxNumChildUsers 4
#define kMaxNumUsers kMaxNumAdminUsers+kMaxNumChildUsers

@interface JVUserDataContainer : JVDataContainer

@property (nonatomic, strong) NSMutableArray* adminDataArray;
@property (nonatomic, strong) NSMutableArray* childDataArray;
@property (nonatomic) NSUInteger count;



@end
