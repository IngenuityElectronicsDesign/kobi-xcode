//
//  JVPinViewController.m
//  Jeeves
//
//  Created by Simon on 10/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVPinViewController.h"

#define kTallPinScreenHeight 300
#define kShortPinScreenHeight 255

@interface JVPinViewController ()

@property (nonatomic, strong) JVPinView* pinView;

@end

@implementation JVPinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    // Setup Pin View
    _pinView = [[JVPinView alloc] initWithFrame:CGRectMake(0, [[self view] frame].size.height-([[JVShared s] isWidescreen] ? kTallPinScreenHeight : kShortPinScreenHeight), [[self view] frame].size.width, ([[JVShared s] isWidescreen] ? kTallPinScreenHeight : kShortPinScreenHeight)) isOverriding:YES];
    [_pinView resetPinDigitsAndOverride];
    [_pinView setTintColor:[[JVShared s] systemTintColor]];
    [_pinView setDelegate:self];
    [[self view] addSubview:_pinView];
}

- (void)passPinCode:(JVPinCodeData*)pinCode
{
    _pinCode = pinCode;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)enteredCorrectPIN
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)enteredIncorrectPIN
{
    [self setTitle:@"Enter New PIN"];
    [_pinView resetPinDigitsAndOverride];
}

#pragma mark - PinView Delegate

- (void)didEnterFirstNewPin:(JVPinCodeData*)pinCode
{
    if( [[JVShared s] isPinConflicted:pinCode] )
    {
        [self setTitle:@"PIN Already Taken"];
        [self performSelector:@selector(enteredIncorrectPIN) withObject:nil afterDelay:0.75f];
        return;
    }
    
    [self setTitle:@"Confirm New PIN"];
    [_pinView resetPinDigits];
}

- (void)didEnterSecondNewPinWithSuccess:(BOOL)success
{
    if( success )
    {
        [self setTitle:@"New PIN Saved"];
        [_pinCode setPinWithString:[[_pinView overridingPinCode] getPinAsString]];
        [self performSelector:@selector(enteredCorrectPIN) withObject:nil afterDelay:0.75f];
    }
    else
    {
        [self setTitle:@"PINs Didn't Match"];
        [self performSelector:@selector(enteredIncorrectPIN) withObject:nil afterDelay:0.75f];
    }
}

@end
