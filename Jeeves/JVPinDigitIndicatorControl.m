//
//  JVPinDigitIndicatorControl.m
//  Jeeves
//
//  Created by Simon on 3/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVPinDigitIndicatorControl.h"

@implementation JVPinDigitIndicatorControl

- (id)initWithFrame:(CGRect)frame
{
    if( self = [super initWithFrame:frame] )
    {
        [self setTintColor:[[JVShared s] systemTintColor]];
        
        self.layer.borderWidth = 2.f;
        self.layer.cornerRadius = self.bounds.size.width/2;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [[self tintColor] CGColor];
    }
    return self;
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:(BOOL)enabled];
    
    [UIView animateWithDuration:(enabled ? 0.1f : 0.25f) delay:0.0 options:(UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction) animations:
     ^{
         if( enabled )
         {
             self.layer.backgroundColor = [[self tintColor] CGColor];
         }
         else
         {
             self.layer.backgroundColor = [[UIColor clearColor] CGColor];
         }
     } completion:
     ^(BOOL finished){ }
     ];
}

#pragma mark - Getters and Setters

- (void)setTintColor:(UIColor *)tintColor
{
    [super setTintColor:tintColor];
    self.layer.borderColor = [tintColor CGColor];
    if( [self isEnabled] )
    {
        self.layer.backgroundColor = [[self tintColor] CGColor];
    }
}

@end
