//
//  JVAppDelegate.h
//  Jeeves
//
//  Created by Simon on 10/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
