//
//  JVDeviceClassDataContainer.h
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVDataContainer.h"
#import "JVDeviceClassData.h"

#define kMaxNumDeviceClasses 6

@interface JVDeviceClassDataContainer : JVDataContainer

@property (nonatomic, strong) NSMutableArray* dataArray;
@property (nonatomic) NSUInteger count;

- (BOOL)addDeviceClassWithName:(NSString*)name andIcon:(UIImage*)icon;
- (BOOL)removeDeviceClassAtIndex:(NSUInteger)index;

- (NSArray*)dataArrayForIndexSet:(NSIndexSet*)indexSet;
- (JVDeviceClassData*)deviceClassForIndex:(NSUInteger)index;

@end
