//
//  XIWindowView.h
//  eXtended Interface
//
//  Created by Simon Narai on 15/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//
//  This is a view that can be dragged around
//  or resized using the bottom edge.
//

#import <Foundation/Foundation.h>


@interface XIWindowView : UIView
{
    CGPoint touchStart;
    BOOL isResizing;
}

@property (nonatomic, retain) UIView *contentView;

@end
