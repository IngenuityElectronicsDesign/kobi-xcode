//
//  JVSettingsTableViewController.h
//  Jeeves
//
//  Created by Simon on 21/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVSettingsTableViewController : UITableViewController

@end
