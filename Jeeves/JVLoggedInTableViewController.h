//
//  JVLoggedInTableViewController.h
//  Jeeves
//
//  Created by Simon on 29/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVLoggedInTableViewController : UITableViewController

@end
