//
//  JVAssignQuotaTableViewController.m
//  Jeeves
//
//  Created by Simon on 28/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVAssignQuotaTableViewController.h"

@interface JVAssignQuotaTableViewController ()

@property (nonatomic) BOOL emergencyHoursEnabled;

@end

@implementation JVAssignQuotaTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    if( self = [super initWithStyle:style] )
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self tableView] registerClass:[JVUserSelectionCell class] forCellReuseIdentifier:@"UserSelectionCell"];
    [[self tableView] registerClass:[JVDeviceClassSelectionCell class] forCellReuseIdentifier:@"DeviceClassSelectionCell"];
    [[self tableView] registerClass:[JVTimeInputCell class] forCellReuseIdentifier:@"TimeInputCell"];
    
    [self refreshView];
    
    _emergencyHoursEnabled = YES;
    
    _dayNamesByIndex = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
}

- (void)refreshView
{
    [_globalTimePeriodSegmentedControl setSelectedSegmentIndex:([[[JVShared s] kobiGlobalSettings] quotaTimePeriodIsDaily] ? 1 : 0)];
    
    [_kobiModeSegmentedControl setSelectedSegmentIndex:[[[JVShared s] kobiQuotaDataContainer] modeForUserIndexSet:[[JVShared s] selectedUsersIndexSet] deviceClassIndexSet:[[JVShared s] selectedDeviceClassesIndexSet]]];
    
    [self compareUsers];
    
    [super refreshView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)globalTimePeriodSegmentedControlValueChanged:(UISegmentedControl *)sender
{
    [[[JVShared s] kobiGlobalSettings] setQuotaTimePeriodIsDaily:[@([sender selectedSegmentIndex]) boolValue]];
    
    [self refreshView];
}

- (IBAction)kobiModeSegmentedControlChanged:(UISegmentedControl *)sender
{
    [[[JVShared s] kobiQuotaDataContainer] setMode:(KobiMode)[sender selectedSegmentIndex]  forUserIndexSet:[[JVShared s] selectedUsersIndexSet] deviceClassIndexSet:[[JVShared s] selectedDeviceClassesIndexSet]];
    
    _needsSaving = YES;

    [self refreshView];
}

- (IBAction)emergencyHoursEnabledSwitchChanged:(UISwitch *)sender
{
    _emergencyHoursEnabled = [sender isOn];
    
    if( [sender isOn] )
    {
        [_emergencyHoursEnabledLabel setText:@"Emergency Hours Enabled"];
    }
    else
    {
        [_emergencyHoursEnabledLabel setText:@"Emergency Hours Disabled"];
    }
    
    [[self tableView] reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if( [self selectedDataSetsCount] > 0 )
    {
        if( [_kobiModeSegmentedControl selectedSegmentIndex] == 1 )
        {
            return 5;
        }
        return 3;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch( section )
    {
        case 0:
        {
            return 1;
        }
        case 1:
        {
            return 2;
        }
        case 2:
        {
            return 1;
        }
        case 3:
        {
            if( [[[JVShared s] kobiGlobalSettings] quotaTimePeriodIsDaily] )
            {
                return 7;
            }
            else
            {
                return 1;
            }
        }
        case 4:
        {
            if( _emergencyHoursEnabled )
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if( indexPath.section == 1 )
    {
        if( indexPath.row == 0 )
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"UserSelectionCell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [(JVUserSelectionCell*)cell setDelegate:self];
        }
        else if( indexPath.row == 1 )
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DeviceClassSelectionCell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [(JVDeviceClassSelectionCell*)cell setDelegate:self];
        }
    }
    else if( indexPath.section == 3 )
    {
        if( [[[JVShared s] kobiGlobalSettings] quotaTimePeriodIsDaily] )
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"TimeInputCell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [(JVTimeInputCell*)cell setupCellWithTitle:[NSString stringWithFormat:@"Hours for %@", _dayNamesByIndex[indexPath.row]] andMaxHours:24];
            [(JVTimeInputCell*)cell setDelegate:self];
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"TimeInputCell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [(JVTimeInputCell*)cell setupCellWithTitle:@"Hours per Week"];
            [(JVTimeInputCell*)cell setDelegate:self];
        }
    }
    else
    {
        cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
        {
            return @"Select Global Time Period";
        }
        case 1:
        {
            return @"Select Children and Device Classes";
        }
        case 2:
        {
            if( [_kobiModeSegmentedControl selectedSegmentIndex] == KOBI_MODE_CONFLICT )
            {
                return @"Override Kobi Mode";
            }
            return @"Select Kobi Mode";
        }
        case 3:
        {
            if( [[[JVShared s] kobiGlobalSettings] quotaTimePeriodIsDaily] )
            {
                return @"Assign Daily Usage Quotas";
            }
            else
            {
                return @"Assign Weekly Usage Quota";
            }
        }
        case 4:
        {
            return @"Assign Emergency Hours";
        }
    }
    return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if( section == 1 )
    {
        if( [self selectedDataSetsCount] == 0 )
        {
            BOOL userSelected = ([[[JVShared s] selectedUsersIndexSet] firstIndex] != NSNotFound);
            BOOL deviceClassSelected = ([[[JVShared s] selectedDeviceClassesIndexSet] firstIndex] != NSNotFound);
            
            if( !userSelected && !deviceClassSelected )
            {
                return @"Tap to select one or more children, and one or more device classes.";
            }
            else if( userSelected && !deviceClassSelected )
            {
                return @"Tap to select one or more device classes.";
            }
            else if( !userSelected && deviceClassSelected )
            {
                return @"Tap to select one or more children.";
            }
        }
    }
    else if( section == 2 )
    {
        switch( [_kobiModeSegmentedControl selectedSegmentIndex] )
        {
            case 0:
            {
                return @"Monitor allows unrestricted use of Kobis.";
            }
            case 2:
            {
                return @"Suspention prevents all use of Kobis.";
            }
        }
    }
    else if( section == 3 )
    {
        return @"Minutes will be rounded to quarter hours.";
    }
    return @"";
}

#pragma mark - SelectionCell Delegates

- (void)userSelectionDidChange
{
    [self selectionCellChange];
}

- (void)deviceClassSelectionDidChange
{
    [self selectionCellChange];
}

- (void)selectionCellChange
{
    [self refreshView];
}

#pragma mark - TimeInputCell Delegates

- (void)timeValueDidChangeToQuotaChar:(unsigned char)quotaChar
{
    int h, m;
    [JVTimeInputCell hours:&h andMinutes:&m fromQuotaChar:quotaChar];
    
    NSLog(@"%d ... %d : %d", quotaChar, h, m);
}

#pragma mark - Parent Methods

- (void)saveButtonPressed
{
    _needsSaving = NO;
    
    [self compareUsers];
}

- (void)discardButtonPressed
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)compareUsers
{
    [super dataModified:_needsSaving];
}

#pragma mark - Getters and Setters

- (int)selectedDataSetsCount
{
    return (int)[[[JVShared s] kobiQuotaDataContainer] dataArrayForUserIndexSet:[[JVShared s] selectedUsersIndexSet] deviceClassIndexSet:[[JVShared s] selectedDeviceClassesIndexSet]].count;
}

@end
