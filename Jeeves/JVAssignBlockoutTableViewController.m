//
//  JVAssignBlockoutTableViewController.m
//  Jeeves
//
//  Created by Simon on 28/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVAssignBlockoutTableViewController.h"

@interface JVAssignBlockoutTableViewController ()

@property (nonatomic) int selectedBlockIndex;

@end

@implementation JVAssignBlockoutTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _selectedBlockIndex = -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch( section )
    {
        case 0:
        {
            return 2;
        }
        case 1:
        {
            if( _selectedBlockIndex > -1 )
            {
                return 4;
            }
            else
            {
                return 2;
            }
        }
        case 2:
        {
            if( _selectedBlockIndex > -1 )
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
    }
    return 0;
}

- (IBAction)blockoutButton1Pressed:(UIButton *)sender
{
    [self blockoutButtonPressedWithIndex:0];
}

- (IBAction)blockoutButton2Pressed:(UIButton *)sender
{
    [self blockoutButtonPressedWithIndex:1];
}

- (void)blockoutButtonPressedWithIndex:(int)index
{
    for( int i = 0; i < [_blockoutButtons count]; i++ )
    {
        UIButton *button = _blockoutButtons[i];
        
        if( i == index )
        {
            [button setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:0.5f]];
        }
        else
        {
            [button setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1.0f]];
        }
    }
    
    if( index == 0 )
    {
        [_startTimeHours setText:@"15"];
        [_startTimeMinutes setText:@"00"];
        [_endTimeHours setText:@"16"];
        [_endTimeMinutes setText:@"30"];
    }
    else
    {
        [_startTimeHours setText:@"18"];
        [_startTimeMinutes setText:@"00"];
        [_endTimeHours setText:@"21"];
        [_endTimeMinutes setText:@"00"];
    }
    
    _selectedBlockIndex = index;
    [[self tableView] reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section == 2 && indexPath.row == 0 )  // Add
    {
        UIButton *button = _blockoutButtons[0];
        
        if( ![button isHidden] )
        {
            button = _blockoutButtons[1];
            [self blockoutButtonPressedWithIndex:1];
        }
        else
        {
            [self blockoutButtonPressedWithIndex:0];
        }
        
        [button setHidden:NO];
    }
    if( indexPath.section == 2 && indexPath.row == 1 )  // Remove
    {
        UIButton *button = _blockoutButtons[_selectedBlockIndex];
        
        if( _selectedBlockIndex == 0 )
        {
            [self blockoutButtonPressedWithIndex:1];
        }
        else
        {
            [self blockoutButtonPressedWithIndex:0];
        }
        
        [button setHidden:YES];
        
        BOOL allHidden = YES;
        
        for( int i = 0; i < [_blockoutButtons count]; i++ )
        {
            UIButton *button = _blockoutButtons[i];
            if( ![button isHidden] )
            {
                allHidden = NO;
            }
        }
        if( allHidden )
        {
            _selectedBlockIndex = -1;
            [[self tableView] reloadData];
        }
    }
    
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
}


/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
