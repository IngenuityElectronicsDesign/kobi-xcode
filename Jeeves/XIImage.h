//
//  XIImage.h
//  eXtended Interface
//
//  Created by Simon on 16/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImage (XIImage)

- (UIImage *)imageWithTintColor:(UIColor *)color;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
