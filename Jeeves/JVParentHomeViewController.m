//
//  JVParentHomeViewController.m
//  Jeeves
//
//  Created by Simon on 16/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVParentHomeViewController.h"
#import "MLPSpotlight.h"

#define kMenuItemCount 6

@interface JVParentHomeViewController ()

@end

@implementation JVParentHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if( self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil] )
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _menuIcons = [[NSMutableArray alloc] init];
    
    NSArray* titles = @[@"Kobi", @"Family", @"Quotas", @"Schedule", @"Usage", @"Hours"];
    NSArray* imageNames = @[@"Kobi.png", @"Children.png", @"Quota.png", @"Schedule.png", @"Usage.png", @"Hours.png"];
    
    for( int i = 0; i < kMenuItemCount; i++ )
    {
        JVMenuIconView* menuIcon = [[JVMenuIconView alloc] initWithFrame:CGRectMake(0, 0, kIconWidth, kIconHeight) title:titles[i] image:[UIImage imageNamed:imageNames[i]] andIndex:i];
        [menuIcon setDelegate:self];
        [[self view] addSubview:menuIcon];
        [_menuIcons addObject:menuIcon];
    }
    
    [self layoutUserIcons];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[[self navigationController] navigationBar] setBarTintColor:[[JVShared s] systemTintColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [[[self navigationController] navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],UITextAttributeTextColor, nil]];
}

- (IBAction)logOutButtonPressed:(UIBarButtonItem *)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)helpButtonPressed:(UIBarButtonItem *)sender
{
    UIButton* button = _menuIcons[1];
    CGPoint point = CGPointMake(button.frame.origin.x + (button.frame.size.width/2), button.frame.origin.y + (button.frame.size.height/2));
    [MLPSpotlight addSpotlightInView:[self view] atPoint:point withStartRadius:90 andEndRadius:96];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Logic

- (void)layoutUserIcons
{
    int x, y, vertSpacing;
    
    vertSpacing = ( [[JVShared s] isWidescreen] ? 31 : 9 );
    
    for( int i = 0; i < kMenuItemCount; i++ )
    {
        JVMenuIconView* menuIcon = _menuIcons[i];
        
        x = kIconEdgePadding + ((kIconWidth+kIconHorizSpacing)*(i%2));
        y = kTitleBarHeight + vertSpacing + ((kIconHeight+vertSpacing)*(i/2));
        
        [menuIcon setFrame:CGRectMake(x, y, [menuIcon frame].size.width, [menuIcon frame].size.height)];
    }
}

#pragma mark - IconView Delegate

- (void)didPressIconWithIndex:(int)index
{
    NSString* pushString = @"";
    
    switch( index )
    {
        case 0:
        {
            pushString = @"ParentKobi";
            break;
        }
        case 1:
        {
            pushString = @"ManageFamilyVC";
            break;
        }
        case 2:
        {
            pushString = @"AssignQuotaTVC";
            break;
        }
        case 3:
        {
            pushString = @"AssignBlockoutTVC";
            break;
        }
        case 4:
        {
            pushString = @"ViewChildrensUsageTVC";
            break;
        }
        case 5:
        {
            pushString = @"ModifyHoursTVC";
            break;
        }
    }
    
    UIViewController* toPush = [[[JVShared s] storyboard] instantiateViewControllerWithIdentifier:pushString];
    
    [[self navigationController] pushViewController:toPush animated:YES];
}

@end
