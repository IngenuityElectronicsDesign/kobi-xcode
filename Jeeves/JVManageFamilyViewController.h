//
//  JVManageFamilyViewController.h
//  Jeeves
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XIImage.h"
#import "XIImageView.h"
#import "JVUserIconView.h"
#import "JVEditUserTableViewController.h"

@interface JVManageFamilyViewController : UIViewController <JVIconViewDelegate>

@property (strong, nonatomic) NSMutableArray* userIcons;

@end
