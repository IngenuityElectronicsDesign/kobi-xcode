//
//  JVPinButton.m
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVPinButton.h"

@implementation UIButton (JVButton)

- (void)circularPinStyle
{
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = self.bounds.size.width/2;
    self.layer.masksToBounds = YES;
    [self setAdjustsImageWhenHighlighted:NO];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:28.f]];
}

- (UIImage *)buttonImageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (void)setTintColor:(UIColor *)tintColor
{
    [super setTintColor:tintColor];
    
    self.layer.borderColor = [tintColor CGColor];
    
    CGFloat r, g, b;
    
    [tintColor getRed:&r green:&g blue:&b alpha:nil];
    
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor colorWithRed:r green:g blue:b alpha:0.2f]] forState:UIControlStateHighlighted];
}

@end
