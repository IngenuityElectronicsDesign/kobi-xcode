//
//  JVData.m
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVData.h"

@implementation JVData

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts
{
    if( !otherData || ![otherData isKindOfClass:[self class]] )
    {
        [conflicts addObject:@(kBasicConflict)];
        return NO;
    }
    
    return YES;
}

@end
