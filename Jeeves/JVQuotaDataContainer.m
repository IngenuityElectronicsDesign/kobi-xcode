//
//  JVQuotaDataContainer.m
//  Kobi
//
//  Created by Simon on 13/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVQuotaDataContainer.h"

@implementation JVQuotaDataContainer

- (id)init
{
    if( self = [super init] )
    {
        NSUInteger userCount = [[JVShared s] childUsersArray] != nil ? [[[JVShared s] childUsersArray] count] : 0;
        NSUInteger deviceClassCount = [[JVShared s] childUsersArray] != nil ? [[[JVShared s] deviceClassDataContainer] count] : 0;
        
        _dataArray = [[NSMutableArray alloc] init];
        
        for( int userIndex = 0; userIndex < userCount; userIndex++ )
        {
            NSMutableArray* deviceClassArray = [[NSMutableArray alloc] init];
            for( int deviceClassIndex = 0; deviceClassIndex < deviceClassCount; deviceClassIndex++ )
            {
                [deviceClassArray addObject:[[JVQuotaData alloc] init]];
            }
            [_dataArray addObject:deviceClassArray];
        }
    }
    return self;
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet *)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVQuotaDataContainer* other = otherData;
        
        for( int i = 0; i < [[self dataArray] count]; i++ )
        {
            for( int j = i; j < [[other dataArray] count]; j++ )
            {
                if( i >= [[self dataArray] count] || j >= [[other dataArray] count] )
                {
                    continue;
                }
                
                JVQuotaData* thisData = [self completeDataArray][i];
                JVQuotaData* oData = [other completeDataArray][j];
                
                if( ![thisData isEqualToOther:oData withConflictsArray:conflicts] )
                {
                    isEqual = NO;
                }
            }
        }
        
        return isEqual;
    }
    return NO;
}

- (NSArray*)completeDataArray;
{
    NSUInteger userCount = [[JVShared s] childUsersArray] != nil ? [[[JVShared s] childUsersArray] count] : 0;
    NSUInteger deviceClassCount = [[JVShared s] childUsersArray] != nil ? [[[JVShared s] deviceClassDataContainer] count] : 0;
    
    NSMutableArray* returnArray = [[NSMutableArray alloc] init];
    
    for( int userIndex = 0; userIndex < userCount; userIndex++ )
    {
        for( int deviceClassIndex = 0; deviceClassIndex < deviceClassCount; deviceClassIndex++ )
        {
            [returnArray addObject:_dataArray[userIndex][deviceClassIndex]];
        }
    }
    
    return [NSArray arrayWithArray:returnArray];
}

- (NSArray*)dataArrayForUserIndexSet:(NSIndexSet*)userIndexSet deviceClassIndexSet:(NSIndexSet*)deviceClassIndexSet
{
    NSUInteger userCount = [[JVShared s] childUsersArray] != nil ? [[[JVShared s] childUsersArray] count] : 0;
    NSUInteger deviceClassCount = [[JVShared s] childUsersArray] != nil ? [[[JVShared s] deviceClassDataContainer] count] : 0;
    
    NSMutableArray* returnArray = [[NSMutableArray alloc] init];
    
    for( int userIndex = 0; userIndex < userCount; userIndex++ )
    {
        if( [userIndexSet containsIndex:userIndex] )
        {
            for( int deviceClassIndex = 0; deviceClassIndex < deviceClassCount; deviceClassIndex++ )
            {
                if( [deviceClassIndexSet containsIndex:deviceClassIndex] )
                {
                    [returnArray addObject:_dataArray[userIndex][deviceClassIndex]];
                }
            }
        }
    }
    
    return [NSArray arrayWithArray:returnArray];
}

- (KobiMode)modeForUserIndexSet:(NSIndexSet*)userIndexSet deviceClassIndexSet:(NSIndexSet*)deviceClassIndexSet
{
    NSArray* dataArray = [self dataArrayForUserIndexSet:userIndexSet deviceClassIndexSet:deviceClassIndexSet];
    
    if( [dataArray count] <= 0 )
    {
        return KOBI_MODE_CONFLICT;
    }
    
    if( [self areModesConflictedForDataArray:dataArray] )
    {
        return KOBI_MODE_CONFLICT;
    }
    
    return [(JVQuotaData*)dataArray[0] mode];
}

- (BOOL)areModesConflictedForDataArray:(NSArray*)dataArray
{
    BOOL retVal = NO;
    
    for( int i = 0; i < [dataArray count]; i++ )
    {
        for( int j = i+1; j < [dataArray count]; j++ )
        {
            if( i >= [dataArray count] || j >= [dataArray count] )
            {
                continue;
            }
            
            JVQuotaData* quota1 = dataArray[i];
            JVQuotaData* quota2 = dataArray[j];
            
            if( [quota1 mode] != [quota2 mode] )
            {
                retVal = YES;
            }
        }
    }
    
    return retVal;
}

- (void)setMode:(KobiMode)mode forUserIndexSet:(NSIndexSet*)userIndexSet deviceClassIndexSet:(NSIndexSet*)deviceClassIndexSet
{
    NSArray* dataArray = [self dataArrayForUserIndexSet:userIndexSet deviceClassIndexSet:deviceClassIndexSet];
    
    if( [dataArray count] <= 0 )
    {
        return;
    }
    
    for( int i = 0; i < [dataArray count]; i++ )
    {
        JVQuotaData* data = dataArray[i];
        [data setMode:mode];
    }
    
    NSLog(@"Setting: %d", [dataArray count]);
}

@end
