//
//  JVTableViewController.m
//  Kobi
//
//  Created by Simon on 18/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVTableViewController.h"

@interface JVTableViewController ()

@end

@implementation JVTableViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataNeedsRefresh:) name:kSharedDataNeedsRefresh object:nil];
    
    [self refreshView];
}

- (void)refreshView
{
    [[self tableView] reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

#pragma mark - Notification Handling

- (void)dataNeedsRefresh:(NSNotification*)notification
{
    [self refreshView];
}

@end
