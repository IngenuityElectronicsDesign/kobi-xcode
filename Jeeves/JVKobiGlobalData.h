//
//  JVKobiGlobalSettings.h
//  Kobi
//
//  Created by Simon on 8/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVData.h"

typedef NS_ENUM(char, KobiGlobalDataConflictTypes)
{
    KobiGlobalDataConflictType_QuotaTimePeriod = 0
};


@interface JVKobiGlobalData : JVData

@property (nonatomic) BOOL quotaTimePeriodIsDaily;

@end
