//
//  JVDeviceClassCell.m
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVDeviceClassCell.h"
#define kMaxDeviceClassNameLength 12

@implementation JVDeviceClassCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if( self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] )
    {
        _classIconView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 6, 32, 32)];
        [_classIconView setHidden:YES];
        [self addSubview:_classIconView];
        
        _classIconButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 6, 32, 32)];
        [_classIconButton setHidden:YES];
        [self addSubview:_classIconButton];
        
        _classNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(62, 0, 196, 44)];
        [_classNameLabel setFont:[UIFont systemFontOfSize:18.0f]];
        [_classNameLabel setHidden:YES];
        [self addSubview:_classNameLabel];
        
        _classNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(62, 0, 196, 44)];
        [_classNameTextField setFont:[UIFont systemFontOfSize:17.0f]];
        [_classNameTextField setPlaceholder:@"Add New Class..."];
        [_classNameTextField setReturnKeyType:UIReturnKeyDone];
        [_classNameTextField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
        [_classNameTextField setDelegate:self];
        [_classNameTextField setHidden:YES];
        [self addSubview:_classNameTextField];
    }
    return self;
}

- (void)setupWithDeviceClass:(JVDeviceClassData*)deviceClass index:(int)index andIsInEditMode:(BOOL)isEditing;
{
    if( deviceClass != nil )
    {
        [_classIconView setImage:[deviceClass icon]];
        [_classIconButton setImage:[deviceClass icon] forState:UIControlStateNormal];
        [_classIconButton setImage:[deviceClass icon] forState:UIControlStateSelected];
        [_classNameLabel setText:[deviceClass name]];
        [_classNameTextField setText:[deviceClass name]];
    }
    else
    {
        [_classIconView setImage:nil];
        [_classIconButton setImage:nil forState:UIControlStateNormal];
        [_classIconButton setImage:nil forState:UIControlStateSelected];
        [_classNameLabel setText:@""];
        [_classNameTextField setText:@""];
    }
    
    [self setIndex:index];
    [self setEditMode:isEditing];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEditMode:(BOOL)editMode
{
    _editMode = editMode;
    
    [_classIconView setHidden:_editMode];
    [_classIconButton setHidden:!_editMode];
    [_classNameLabel setHidden:_editMode];
    [_classNameTextField setHidden:!_editMode];
}

- (void)textFieldValueChanged:(UITextField*)textField
{
    NSRange stringRange = {0, MIN( [[textField text] length], kMaxDeviceClassNameLength )};
    [textField setText:[[textField text] substringWithRange:stringRange]];
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:[textField keyboardType] == UIKeyboardTypeNumberPad];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [[self delegate] deviceClassWithIndex:_index didChangeName:[textField text]];
}

@end
