//
//  JVChildHomeViewController.h
//  Jeeves
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XIImage.h"
#import "XIImageView.h"

@interface JVChildHomeViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *menuButtons;

@end
