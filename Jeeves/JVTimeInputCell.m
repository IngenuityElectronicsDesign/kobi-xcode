//
//  JVTimeInputCell.m
//  Kobi
//
//  Created by Simon on 14/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVTimeInputCell.h"
#define kRounding 15
#define kMaxHours 60

@implementation JVTimeInputCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if( self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] )
    {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 210, 44)];
        [_titleLabel setFont:[UIFont systemFontOfSize:17.f]];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:[UIColor blackColor]];
        [self addSubview:_titleLabel];
        
        _seperatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(265, 10, 10, 21)];
        [_seperatorLabel setFont:[UIFont systemFontOfSize:17.f]];
        [_seperatorLabel setTextAlignment:NSTextAlignmentCenter];
        [_seperatorLabel setTextColor:[UIColor blackColor]];
        [_seperatorLabel setText:@":"];
        [self addSubview:_seperatorLabel];
        
        _hourTextField = [[UITextField alloc] initWithFrame:CGRectMake(235, 0, 30, 44)];
        [_hourTextField setFont:[UIFont systemFontOfSize:17.f]];
        [_hourTextField setTextAlignment:NSTextAlignmentRight];
        [_hourTextField setTextColor:[[JVShared s] systemTintColor]];
        [_hourTextField setPlaceholder:@"hh"];
        [_hourTextField setText:@"00"];
        [_hourTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [_hourTextField setClearsOnBeginEditing:YES];
        [_hourTextField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
        [_hourTextField setDelegate:self];
        [self addSubview:_hourTextField];
        
        _minuteTextField = [[UITextField alloc] initWithFrame:CGRectMake(275, 0, 30, 44)];
        [_minuteTextField setFont:[UIFont systemFontOfSize:17.f]];
        [_minuteTextField setTextAlignment:NSTextAlignmentLeft];
        [_minuteTextField setTextColor:[[JVShared s] systemTintColor]];
        [_minuteTextField setPlaceholder:@"mm"];
        [_minuteTextField setText:@"00"];
        [_minuteTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [_minuteTextField setClearsOnBeginEditing:YES];
        [_minuteTextField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
        [_minuteTextField setDelegate:self];
        [self addSubview:_minuteTextField];
        
        _maxHours = kMaxHours;
    }
    return self;
}

- (void)setupCellWithTitle:(NSString *)title
{
    [self setupCellWithTitle:title andMaxHours:kMaxHours];
}

- (void)setupCellWithTitle:(NSString*)title andMaxHours:(int)maxHours
{
    [_titleLabel setText:title];
    _maxHours = MIN(maxHours, kMaxHours);
}

- (void)textFieldValueChanged:(UITextField*)textField
{
    NSRange stringRange = {0, MIN( [[textField text] length], 2 )};
    [textField setText:[[textField text] substringWithRange:stringRange]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:NO];
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:[textField keyboardType] == UIKeyboardTypeNumberPad];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    int val = [[textField text] intValue];
    
    if( textField == _hourTextField )
    {
        _hourValue = val;
        if( _hourValue > _maxHours )
        {
            _hourValue = _maxHours;
        }
        if( _hourValue == _maxHours && _minuteValue != 0 )
        {
            _hourValue = _maxHours-1;
        }
        
        [textField setText:[NSString stringWithFormat:@"%02d", _hourValue]];
    }
    else if( textField == _minuteTextField )
    {
        _minuteValue = [self roundNumber:val to:(float)kRounding];
        if( _minuteValue >= 60 )
        {
            _minuteValue = 0;
        }
        
        [textField setText:[NSString stringWithFormat:@"%02d", _minuteValue]];
        
        if( [[_hourTextField text] intValue] == _maxHours && _minuteValue != 0 )
        {
            _hourValue = _maxHours-1;
            [_hourTextField setText:[NSString stringWithFormat:@"%02d", _hourValue]];
        }
    }

    [[self delegate] timeValueDidChangeToQuotaChar:[JVTimeInputCell quotaCharFromHours:_hourValue andMinutes:_minuteValue]];
}

#pragma mark - Util Funcs

- (float)roundNumber:(float)number to:(float)to
{
    if( number >= 0 )
    {
        return to * floorf(number / to + 0.5f);
    }
    else
    {
        return to * ceilf(number / to - 0.5f);
    }
}

+ (unsigned char)quotaCharFromHours:(int)hours andMinutes:(int)mins
{
    return ((hours * 4) + (((float)mins/60) * 4));
}

+ (void)hours:(int*)hours andMinutes:(int*)mins fromQuotaChar:(unsigned char)quotaChar
{
    *hours = quotaChar / 4;
    *mins = (quotaChar % 4)*kRounding;
}

@end
