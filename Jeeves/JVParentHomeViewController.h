//
//  JVParentHomeViewController.h
//  Jeeves
//
//  Created by Simon on 16/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVMenuIconView.h"
#import "XIImageView.h"
#import "XIImage.h"

@interface JVParentHomeViewController : UIViewController <JVIconViewDelegate>

@property (nonatomic, strong) NSMutableArray* menuIcons;

@end
