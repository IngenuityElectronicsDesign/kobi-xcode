//
//  JVIconView.h
//  Jeeves
//
//  Created by Simon on 9/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JVIconViewDelegate <NSObject>

- (void)didPressIconWithIndex:(int)index;

@end

@interface JVIconView : UIView

@property (nonatomic, weak) id<JVIconViewDelegate> delegate;
@property (nonatomic) int index;

@property (nonatomic, strong) UIButton* button;
@property (nonatomic, strong) UILabel* label;

- (id)initWithFrame:(CGRect)frame andIndex:(int)index;

@end


