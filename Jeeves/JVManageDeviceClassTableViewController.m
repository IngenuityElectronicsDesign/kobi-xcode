//
//  JVManageDeviceClassTableViewController.m
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVManageDeviceClassTableViewController.h"

@interface JVManageDeviceClassTableViewController ()

@end

@implementation JVManageDeviceClassTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if( self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil] )
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [[self tableView] registerClass:[JVDeviceClassCell class] forCellReuseIdentifier:@"DeviceClassCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MIN([[[JVShared s] deviceClassDataContainer] count] + 1, kMaxNumDeviceClasses);
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeviceClassCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [(JVDeviceClassCell*)cell setupWithDeviceClass:(indexPath.row == [[[JVShared s] deviceClassDataContainer] count] ? nil : (JVDeviceClassData*)[[[JVShared s] deviceClassDataContainer] deviceClassForIndex:indexPath.row]) index:indexPath.row andIsInEditMode:YES];
    [(JVDeviceClassCell*)cell setDelegate:self];
    
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"Tap to change an icon or to rename a device class. Stick to short, simple names! Swipe to remove a device class.";
}

- (NSString*)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NSString stringWithFormat:@"Remove %@", [(JVDeviceClassData*)[[[JVShared s] deviceClassDataContainer] deviceClassForIndex:indexPath.row] name]];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row != [[[JVShared s] deviceClassDataContainer] count] )
    {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        _deletionIndex = indexPath.row;
        _deleteConfirmationActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure want to remove %@?\n\nThis will permanatly delete this device class, and any associated quotas and schedules.\nThis cannot be undone.", [(JVDeviceClassData*)[[[JVShared s] deviceClassDataContainer ] deviceClassForIndex:indexPath.row] name]] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:[NSString stringWithFormat:@"Remove %@", [(JVDeviceClassData*)[[[JVShared s] deviceClassDataContainer] deviceClassForIndex:indexPath.row] name]] otherButtonTitles:nil];
        [_deleteConfirmationActionSheet showInView:[self view]];
    }
}

#pragma mark - ActionSheet Delegates

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Pressed: %d", buttonIndex);
    
    if( buttonIndex == 0 )
    {
        [[[JVShared s] deviceClassDataContainer] removeDeviceClassAtIndex:_deletionIndex];
    }

    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - DeviceClassCell Delegate

- (void)deviceClassWithIndex:(int)index didChangeName:(NSString *)newName
{
    if( index < [[[JVShared s] deviceClassDataContainer] count] )
    {
        JVDeviceClassData* deviceClass = [[[JVShared s] deviceClassDataContainer] deviceClassForIndex:index];
        [deviceClass setName:newName];
    }
    else
    {
        [[[JVShared s] deviceClassDataContainer] addDeviceClassWithName:newName andIcon:[UIImage imageNamed:@"TV.png"]];
    }
    
    [self refreshView];
}

@end
