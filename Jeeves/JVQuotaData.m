//
//  JVQuotaData.m
//  Kobi
//
//  Created by Simon on 13/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVQuotaData.h"
#define kNumOfDays 7

@implementation JVQuotaData

- (id)init
{
    if( self = [super init] )
    {
        _mode = MONITOR_MODE;
        
        _weeklyQuota = 0;
        
        id numbers[kNumOfDays];
        for( int i = 0; i < kNumOfDays; i++ )
        {
            numbers[i] = [NSNumber numberWithUnsignedChar:0];
        }
        _dailyQuotas = [NSMutableArray arrayWithObjects:numbers count:kNumOfDays];
        
        _emergencyHoursEnabled = NO;
        _emergencyHourQuota = 0;
    }
    return self;
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVQuotaData* other = otherData;
        
        if( [self mode] != [other mode] )
        {
            [conflicts addObject:@(QuotaDataConflictTypes_MODE)];
            isEqual = NO;
        }
        if( [self weeklyQuota] != [other weeklyQuota] )
        {
            [conflicts addObject:@(QuotaDataConflictTypes_WEEKLY_QUOTA)];
            isEqual = NO;
        }
        if( ![[self dailyQuotas] isEqualToArray:[other dailyQuotas]] )
        {
            [conflicts addObject:@(QuotaDataConflictTypes_DAILY_QUOTA)];
            isEqual = NO;
        }
        if( [self emergencyHoursEnabled] != [other emergencyHoursEnabled] )
        {
            [conflicts addObject:@(QuotaDataConflictTypes_EMERGENCY_ENABLED)];
            isEqual = NO;
        }
        if( [self emergencyHourQuota] != [other emergencyHourQuota] )
        {
            [conflicts addObject:@(QuotaDataConflictTypes_EMERGENCY_QUOTA)];
            isEqual = NO;
        }
        
        return isEqual;
    }
    return NO;
}

#pragma mark - Getters and Setters

- (void)setDailyQuotasWithArray:(NSArray *)dailyQuotas
{
    if( [dailyQuotas count] == [_dailyQuotas count] )
    {
        for( int i = 0; i < [_dailyQuotas count]; i++ )
        {
            int val = [dailyQuotas[i] intValue];
            _dailyQuotas[i] = @(val);
        }
    }
}

@end
