//
//  JVKobiGlobalSettings.m
//  Kobi
//
//  Created by Simon on 8/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVKobiGlobalData.h"

@implementation JVKobiGlobalData

- (id)init
{
    if( self = [super init] )
    {
        _quotaTimePeriodIsDaily = NO;
    }
    return self;
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet *)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVKobiGlobalData* other = otherData;
        
        if( [self quotaTimePeriodIsDaily] != [other quotaTimePeriodIsDaily] )
        {
            [conflicts addObject:@(KobiGlobalDataConflictType_QuotaTimePeriod)];
            isEqual = NO;
        }
        
        return isEqual;
    }
    return NO;
}

@end
