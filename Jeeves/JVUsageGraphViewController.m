//
//  JVUsageGraphViewController.m
//  Jeeves
//
//  Created by Simon on 14/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVUsageGraphViewController.h"

@interface JVUsageGraphViewController ()

@property (nonatomic, strong) NSMutableArray* data;

@end

@implementation JVUsageGraphViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _data = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_data addObjectsFromArray:[NSMutableArray arrayWithObjects:@1.0, @1.0, @0.5, @1.0, @2.0, @1.5, @2.0, nil]];
    
    [_barGraphView drawBarsInGraphWithDataArray:_data withMaxHeight:@5.8];
}

- (IBAction)deviceTypeSegmentSwitched:(UISegmentedControl *)sender
{
    [_data removeAllObjects];
    
    switch( [sender selectedSegmentIndex] )
    {
        case 0:
        {
            [_data addObjectsFromArray:[NSMutableArray arrayWithObjects:@1.0, @1.0, @0.5, @1.0, @2.0, @1.5, @2.0, nil]];
            break;
        }
        case 1:
        {
            [_data addObjectsFromArray:[NSMutableArray arrayWithObjects:@0.0, @1.0, @0.0, @0.5, @0.0, @1.0, @1.5, nil]];
            break;
        }
        case 2:
        {
            [_data addObjectsFromArray:[NSMutableArray arrayWithObjects:@2.5, @2.0, @2.5, @2.0, @2.0, @1.0, @1.0, nil]];
            break;
        }
        case 3:
        {
            [_data addObjectsFromArray:[NSMutableArray arrayWithObjects:@3.5, @4.0, @3.0, @3.5, @4.0, @3.5, @4.5, nil]];
            break;
        }
    }
    
    [_barGraphView drawBarsInGraphWithDataArray:_data withMaxHeight:@5.8];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
