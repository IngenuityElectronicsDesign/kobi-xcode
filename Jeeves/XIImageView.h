//
//  XIImageView.h
//  eXtended Interface
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImageView (XIImageView)

- (UIImageView *)imageCroppedToCircleWithBorderThickness:(float)borderThickness andBorderColor:(UIColor*)borderColor;

@end
