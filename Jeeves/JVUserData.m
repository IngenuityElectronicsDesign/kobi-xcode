//
//  JVUser.m
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVUserData.h"

@implementation JVUserData

- (id)initWithName:(NSString*)name portrait:(UIImage*)portrait andColorIndex:(UserTintColorIndex)colorIndex
{
    return [self initWithName:name portrait:portrait colorIndex:colorIndex andIsAdmin:YES];
}

- (id)initWithName:(NSString *)name portrait:(UIImage *)portrait colorIndex:(UserTintColorIndex)colorIndex andIsAdmin:(BOOL)isAdmin
{
    if( self = [super init] )
    {
        _admin = isAdmin;
        _name = name;
        _portrait = portrait;
        _tintColorIndex = colorIndex;
        _pinCode = [[JVPinCodeData alloc] init];
    }
    return self;
}

- (void)setPinCodeWithString:(NSString*)pinCodeString
{
    [_pinCode setPinWithString:pinCodeString];
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVUserData* other = otherData;
        
        if( ![[self name] isEqualToString:[other name]] )
        {
            [conflicts addObject:@(UserConflictType_NAME)];
            isEqual = NO;
        }
        if( ![[self portrait] isEqual:[other portrait]] )
        {
            [conflicts addObject:@(UserConflictType_PORTRAIT)];
            isEqual = NO;
        }
        if( [self tintColorIndex] != [other tintColorIndex] )
        {
            [conflicts addObject:@(UserConflictType_COLOR)];
            isEqual = NO;
        }
        if( ![[self pinCode] isEqualToOther:[other pinCode] withConflictsArray:nil] )
        {
            [conflicts addObject:@(UserConflictType_PIN)];
            isEqual = NO;
        }
        
        return isEqual;
    }
    return NO;
}

- (BOOL)isEqualTo:(JVUserData *)otherUser
{
    BOOL equal = YES;
    
    if( ![_name isEqualToString:[otherUser name]] )
    {
        equal = NO;
    }
    if( ![_portrait isEqual:[otherUser portrait]] )
    {
        equal = NO;
    }
    if( _tintColorIndex != [otherUser tintColorIndex] )
    {
        equal = NO;
    }
    if( ![_pinCode isEqualToPinCode:[otherUser pinCode]] )
    {
        equal = NO;
    }
    
    return equal;
}

- (BOOL)isConflicted:(JVUserData*)otherUser withType:(ConflictType)type
{
    switch( type )
    {
        case CONFLICT_NAME:
        {
            return [_name isEqualToString:[otherUser name]];
            break;
        }
        case CONFLICT_COLOR:
        {
            return _tintColorIndex == [otherUser tintColorIndex];
            break;
        }
    }
}

- (BOOL)isPinConflicted:(JVPinCodeData*)otherPin
{
    return [_pinCode isEqualToPinCode:otherPin];
}

@end
