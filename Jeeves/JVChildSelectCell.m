//
//  JVChildSelectCell.m
//  Jeeves
//
//  Created by Simon on 28/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVChildSelectCell.h"

@implementation JVChildSelectCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        [self setup];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (void)setup
{
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // Get this from data in future
    NSArray *names = @[@"Johnny", @"Sally", @"Billy", @"All"];
    
    _childSelectSegmentedControl = [[UISegmentedControl alloc] initWithItems:names];
    [_childSelectSegmentedControl setFrame:CGRectMake(20, 8, 280, 29)];
    
    [_childSelectSegmentedControl setSelectedSegmentIndex:([_childSelectSegmentedControl numberOfSegments] - 1)];
    
    [_childSelectSegmentedControl addTarget:self action:@selector(childSelected) forControlEvents:UIControlEventValueChanged];
    
    [self addSubview:_childSelectSegmentedControl];
}

- (void)childSelected
{
    
}

@end
