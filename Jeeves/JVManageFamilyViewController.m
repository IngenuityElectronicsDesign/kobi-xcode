//
//  JVManageFamilyViewController.m
//  Jeeves
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVManageFamilyViewController.h"

#define kUserItemCount 6

@interface JVManageFamilyViewController ()

@end

@implementation JVManageFamilyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if( self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil] )
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _userIcons = [[NSMutableArray alloc] init];
    
    for( int i = 0; i < kAdminUsersArrayMaxSize; i++ )
    {
        JVUserIconView* userIcon = nil;
        
        if( i < [[[JVShared s] adminUsersArray] count] )
        {
            userIcon = [[JVUserIconView alloc] initWithFrame:CGRectMake(0, 0, kIconWidth, kIconHeight) userData:[[JVShared s] adminUsersArray][i] andIndex:i];
        }
        else
        {
            userIcon = [[JVUserIconView alloc] initWithFrame:CGRectMake(0, 0, kIconWidth, kIconHeight) blankAsAdmin:YES andIndex:i];
        }
        
        [userIcon setDelegate:self];
        [[self view] addSubview:userIcon];
        [_userIcons addObject:userIcon];
    }
    for( int i = 0; i < kChildUsersArrayMaxSize; i++ )
    {
        JVUserIconView* userIcon = nil;
        
        if( i < [[[JVShared s] childUsersArray] count] )
        {
            userIcon = [[JVUserIconView alloc] initWithFrame:CGRectMake(0, 0, kIconWidth, kIconHeight) userData:[[JVShared s] childUsersArray][i] andIndex:i+kAdminUsersArrayMaxSize];
        }
        else
        {
            userIcon = [[JVUserIconView alloc] initWithFrame:CGRectMake(0, 0, kIconWidth, kIconHeight) blankAsAdmin:NO andIndex:i+kAdminUsersArrayMaxSize];
        }
        
        [userIcon setDelegate:self];
        [[self view] addSubview:userIcon];
        [_userIcons addObject:userIcon];
    }
    
    [self layoutUserIcons];
}

- (void)viewWillAppear:(BOOL)animated
{
    for( int i = 0; i < kAdminUsersArrayMaxSize; i++ )
    {
        if( i < [[[JVShared s] adminUsersArray] count] )
        {
            [_userIcons[i] setWithUserData:[[JVShared s] adminUsersArray][i]];
        }
        else
        {
            [_userIcons[i] setBlankAsAdmin:YES];;
        }
    }
    for( int i = 0; i < kChildUsersArrayMaxSize; i++ )
    {
        if( i < [[[JVShared s] childUsersArray] count] )
        {
            [_userIcons[i+kAdminUsersArrayMaxSize] setWithUserData:[[JVShared s] childUsersArray][i]];
        }
        else
        {
            [_userIcons[i+kAdminUsersArrayMaxSize] setBlankAsAdmin:NO];;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Layout Logic

- (void)layoutUserIcons
{
    int x, y, vertSpacing;
    
    vertSpacing = ( [[JVShared s] isWidescreen] ? 31 : 9 );
    
    for( int i = 0; i < kUserItemCount; i++ )
    {
        JVUserIconView* userIcon = _userIcons[i];
        
        x = kIconEdgePadding + ((kIconWidth+kIconHorizSpacing)*(i%2));
        y = kTitleBarHeight + vertSpacing + ((kIconHeight+vertSpacing)*(i/2));
        
        [userIcon setFrame:CGRectMake(x, y, [userIcon frame].size.width, [userIcon frame].size.height)];
    }
}

#pragma mark - IconView Delegate

- (void)didPressIconWithIndex:(int)index
{
    JVEditUserTableViewController* toPush = [[[JVShared s] storyboard] instantiateViewControllerWithIdentifier:@"editUserTVC"];
    
    if( index < kAdminUsersArrayMaxSize )
    {
        [toPush setIsAdmin:YES];
        [toPush setSelectedUser:[[JVShared s] userDataForIndex:index fromAdmin:YES]];
    }
    else
    {
        [toPush setIsAdmin:NO];
        [toPush setSelectedUser:[[JVShared s] userDataForIndex:index-kAdminUsersArrayMaxSize fromAdmin:NO]];
    }
    
    [[self navigationController] pushViewController:toPush animated:YES];
}

@end
