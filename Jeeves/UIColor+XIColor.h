//
//  UIColor+XIColor.h
//  eXtented Interface
//
//  Created by Simon on 1/11/2013.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

// Color Scheme Creation Enum
typedef enum
{
    ColorSchemeAnalagous = 0,
    ColorSchemeMonochromatic,
    ColorSchemeTriad,
    ColorSchemeComplementary
	
} ColorScheme;

@interface UIColor (XIColor)

// Color Methods
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (UIColor *) colorWithHexNumber:(UInt32)hex andAlpha:(CGFloat)alpha;
+ (UIColor *)colorWithRGBAArray:(NSArray *)rgbaArray;
+ (UIColor *)colorWithCMYK:(NSArray *)cmykValues;
- (NSString *)hexString;
- (NSArray *)rgbaArray;//returns the rgba as percents
- (NSArray *)rgbaValues;//returns rgba as actual rgb values
- (NSArray *)hsbaArray;
- (NSDictionary *)rgbaDict;
- (NSDictionary *)hsbaDict;

// Generate Color Scheme
- (NSArray *)colorSchemeOfType:(ColorScheme)type;

// iOS 7 Colors
+ (instancetype)red7Color;
+ (instancetype)orange7Color;
+ (instancetype)yellow7Color;
+ (instancetype)green7Color;
+ (instancetype)lightBlue7Color;
+ (instancetype)darkBlue7Color;
+ (instancetype)purple7Color;
+ (instancetype)pink7Color;
+ (instancetype)darkGray7Color;
+ (instancetype)lightGray7Color;

// iOS 7 Gradient Colors
+ (instancetype)red7GradientStartColor;
+ (instancetype)red7GradientEndColor;
+ (instancetype)orange7GradientStartColor;
+ (instancetype)orange7GradientEndColor;
+ (instancetype)yellow7GradientStartColor;
+ (instancetype)yellow7GradientEndColor;
+ (instancetype)green7GradientStartColor;
+ (instancetype)green7GradientEndColor;
+ (instancetype)teal7GradientStartColor;
+ (instancetype)teal7GradientEndColor;
+ (instancetype)blue7GradientStartColor;
+ (instancetype)blue7GradientEndColor;
+ (instancetype)violet7GradientStartColor;
+ (instancetype)violet7GradientEndColor;
+ (instancetype)magenta7GradientStartColor;
+ (instancetype)magenta7GradientEndColor;
+ (instancetype)black7GradientStartColor;
+ (instancetype)black7GradientEndColor;
+ (instancetype)silver7GradientStartColor;
+ (instancetype)silver7GradientEndColor;

@end
