//
//  JVShared.h
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+XIColor.h"
#import "JVUserData.h"
#import "JVKobiGlobalData.h"
#import "JVDeviceClassDataContainer.h"
#import "JVQuotaDataContainer.h"

// Max Size Defs
#define kAdminUsersArrayMaxSize 2
#define kChildUsersArrayMaxSize 4
#define kTotalUsersArrayMaxSize kAdminUsersArrayMaxSize+kChildUsersArrayMaxSize
#define kKobiBoxesArrayMaxSize 4

// Icon Layout Defs
#define kTitleBarHeight 60
#define kIconHeight 128
#define kIconWidth 96
#define kIconEdgePadding 43
#define kIconHorizSpacing 42

// Notification Strings
#define kSharedDataNeedsRefresh @"sharedDataNeedsRefresh"

@interface JVShared : NSObject

// System
@property (nonatomic, getter=isWidescreen) BOOL widescreen;
@property (nonatomic, strong) UIStoryboard* storyboard;

// Color
@property (nonatomic, strong) UIColor* systemTintColor;
@property (nonatomic, strong) NSArray* userTintColorsArray;

// Users
@property (nonatomic, strong) NSMutableArray* adminUsersArray;
@property (nonatomic, strong) NSMutableArray* childUsersArray;
@property (nonatomic) int totalUserCount;
@property (nonatomic, strong) JVUserData* loggedInUser;
@property (nonatomic, strong) NSMutableIndexSet* selectedUsersIndexSet;

// Device Classes
@property (nonatomic, strong) JVDeviceClassDataContainer* deviceClassDataContainer;
@property (nonatomic, strong) NSMutableIndexSet* selectedDeviceClassesIndexSet;

// Kobi Boxes
@property (nonatomic, strong) NSMutableArray* kobiBoxesArray;

// Kobi Settings
@property (nonatomic, strong) JVKobiGlobalData* kobiGlobalSettings;

// Kobi Data
@property (nonatomic, strong) JVQuotaDataContainer* kobiQuotaDataContainer;

+ (instancetype)s;

- (void)setupDataStructures;

- (void)syncWithJeevesNetwork;

// User Retrieval
- (JVUserData*)userDataForIndex:(int)index;
- (id)userDataForIndex:(int)index fromAdmin:(BOOL)fromAdmin;

// User Modification
- (BOOL)addUserWithName:(NSString*)name portrait:(UIImage*)portrait colorIndex:(UserTintColorIndex)index andIsAdmin:(BOOL)isAdmin;
- (BOOL)addUserWithUserData:(JVUserData*)newUser;
- (void)removeUserWithUserData:(JVUserData*)userData fromAdmin:(BOOL)fromAdmin;

// User Checking
- (BOOL)isUser:(JVUserData*)otherUser conflictedWithType:(ConflictType)type withExceptedUser:(JVUserData*)exceptedUser;
- (BOOL)isPinConflicted:(JVPinCodeData*)otherPin;

// Child User
//- (BOOL)selectedChildDataOfType:(DataType)type;

// Tint Color Retrieval
- (UIColor*)userTintColorForIndex:(UserTintColorIndex)index;

@end
