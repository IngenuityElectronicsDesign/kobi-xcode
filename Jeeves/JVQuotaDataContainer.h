//
//  JVQuotaDataContainer.h
//  Kobi
//
//  Created by Simon on 13/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVDataContainer.h"
#import "JVQuotaData.h"

@interface JVQuotaDataContainer : JVDataContainer

@property (nonatomic, strong) NSMutableArray* dataArray;

- (NSArray*)completeDataArray;
- (NSArray*)dataArrayForUserIndexSet:(NSIndexSet*)userIndexSet deviceClassIndexSet:(NSIndexSet*)deviceClassIndexSet;

- (KobiMode)modeForUserIndexSet:(NSIndexSet*)userIndexSet deviceClassIndexSet:(NSIndexSet*)deviceClassIndexSet;
- (void)setMode:(KobiMode)mode forUserIndexSet:(NSIndexSet*)userIndexSet deviceClassIndexSet:(NSIndexSet*)deviceClassIndexSet;

@end
