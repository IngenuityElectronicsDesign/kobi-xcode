//
//  JVTimeWarningViewController.m
//  Jeeves
//
//  Created by Simon on 31/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVTimeWarningViewController.h"

@interface JVTimeWarningViewController ()

@property (nonatomic, strong) MYBlurIntroductionView* introView;

@end

@implementation JVTimeWarningViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self buildIntro];
}

- (void)buildIntro
{
    MYIntroductionPanel *panel = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Heads Up - Time is Running Out!" description:@"Hey Johnny,\n\nJust a heads up that you've only got 10 minutes left on your Xbox. Better save the game soon!\n\n- Jeeves" image:[UIImage imageNamed:@"Sad.png"]];
    
    NSArray *panels = @[panel];
    
    _introView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    _introView.delegate = self;
    //_introView.BackgroundImageView.Image
    
    [_introView buildIntroductionWithPanels:panels];
    
    [_introView setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1]];
    
    //[[_introView RightSkipButton] setHidden:YES];
    [[_introView PageControl] setHidden:YES];
    //[[_introView PageControl] setNumberOfPages:[[_introView PageControl] numberOfPages] + 1];
    //[_introView setRightSkipButton:nil];
    
    [[self view] addSubview:_introView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MYIntroduction Delegate

-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex
{
    NSLog(@"Introduction did change to panel %ld", (long)panelIndex);
    
    //[_introView setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1]];
    
    if( panelIndex >= 3 )
    {
        [[_introView RightSkipButton] setHidden:NO];
    }
    else
    {
        [[_introView RightSkipButton] setHidden:YES];
    }
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
