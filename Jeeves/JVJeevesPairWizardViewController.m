//
//  JVJeevesPairWizardViewController.m
//  Jeeves
//
//  Created by Simon on 21/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVJeevesPairWizardViewController.h"

@interface JVJeevesPairWizardViewController ()

@property (nonatomic, strong) MYBlurIntroductionView* introView;

@end

@implementation JVJeevesPairWizardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self buildIntro];
}

- (void)buildIntro
{
    MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Plug Jeeves In" description:@"Plug Jeeves into an outlet where you would like to use it. Ensure it is powered off.\n\nSwipe left to continue."];
    
    MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Turn Jeeve On" description:@"Switch Jeeves on, and it will enter pairing mode."];
    
    MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"Waiting for Jeeves" description:@"Waiting for Jeeves to Pair..." image:[UIImage imageNamed:@"Activity"]];
    
    MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) title:@"That's it!" description:@"The new Jeeves box will now appear in your Jeeves Boxes list.\n\nSwipe left to close."];
    
    NSArray *panels = @[panel1, panel2, panel3, panel4];
    
    _introView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    _introView.delegate = self;
    //_introView.BackgroundImageView.Image
    
    [_introView buildIntroductionWithPanels:panels];
    
    [_introView setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1]];
    
    [[_introView RightSkipButton] setHidden:YES];
    [[_introView PageControl] setEnabled:NO];
    //[[_introView PageControl] setNumberOfPages:[[_introView PageControl] numberOfPages] + 1];
    [_introView setRightSkipButton:nil];
    
    [[self view] addSubview:_introView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MYIntroduction Delegate

-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex
{
    NSLog(@"Introduction did change to panel %ld", (long)panelIndex);
    
    //[_introView setBackgroundColor:[UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1]];
    
    if( panelIndex >= 3 )
    {
        [[_introView RightSkipButton] setHidden:NO];
    }
    else
    {
        [[_introView RightSkipButton] setHidden:YES];
    }
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end