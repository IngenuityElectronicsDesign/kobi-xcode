//
//  JVChildHomeViewController.m
//  Jeeves
//
//  Created by Simon on 17/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVChildHomeViewController.h"

@interface JVChildHomeViewController ()

@end

@implementation JVChildHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for( int i = 0; i < [_menuButtons count]; i++ )
	{
        UIButton* button = _menuButtons[i];
        
        [[button imageView] imageCroppedToCircleWithBorderThickness:2.5f andBorderColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] loggedInUser] tintColorIndex]]];
        
        [button setImage:[[[button imageView] image] imageWithTintColor:[[JVShared s] userTintColorForIndex:[[[JVShared s] loggedInUser] tintColorIndex]]] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self setTitle:[[[JVShared s] loggedInUser] name]];
    [[[self navigationController] navigationBar] setBarTintColor: [[JVShared s] userTintColorForIndex:[[[JVShared s] loggedInUser] tintColorIndex]]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [[[self navigationController] navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],UITextAttributeTextColor, nil]];
}

- (IBAction)logOutButtonPressed:(UIBarButtonItem *)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
