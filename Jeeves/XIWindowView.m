//
//  XIWindowView.m
//  eXtended Interface
//
//  Created by Simon Narai on 15/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//

#import "XIWindowView.h"

#define kResizeThumbSize 15

@implementation XIWindowView

- (void)setContentView:(UIView *)newContentView
{
    [_contentView removeFromSuperview];
    _contentView = newContentView;
    _contentView.frame = self.bounds;
    [self addSubview:_contentView];
}

- (void)setFrame:(CGRect)newFrame
{
    [super setFrame:newFrame];
    _contentView.frame = self.bounds;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    touchStart = [[touches anyObject] locationInView:self];
    isResizing = (self.bounds.size.height - touchStart.y < kResizeThumbSize );
    if( isResizing )
    {
        touchStart = CGPointMake(touchStart.x, touchStart.y - self.bounds.size.height);
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchPoint = [[touches anyObject] locationInView:self];
    if (isResizing)
    {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, touchPoint.y - touchStart.y);
    }
    else
    {
        self.center = CGPointMake(self.center.x + touchPoint.x - touchStart.x, self.center.y + touchPoint.y - touchStart.y);
    }
}

@end
