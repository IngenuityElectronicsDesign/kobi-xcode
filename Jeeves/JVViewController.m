//
//  JVViewController.m
//  Kobi
//
//  Created by Simon on 18/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVViewController.h"

@interface JVViewController ()

@end

@implementation JVViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataNeedsRefresh:) name:kSharedDataNeedsRefresh object:nil];
    
    [self refreshView];
}

- (void)refreshView
{
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

#pragma mark - Notification Handling

- (void)dataNeedsRefresh:(NSNotification*)notification
{
    [self refreshView];
}

@end
