//
//  JVViewUsageTableViewController.m
//  Jeeves
//
//  Created by Simon on 30/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVViewUsageTableViewController.h"
#import "KAProgressLabel.h"

@interface JVViewUsageTableViewController ()

@property (strong, nonatomic) IBOutlet KAProgressLabel *tvUsageRingProgressView;
@property (strong, nonatomic) IBOutlet KAProgressLabel *gamesUsageRingProgressView;
@property (strong, nonatomic) IBOutlet KAProgressLabel *computerUsageRingProgressView;

@property (strong, nonatomic) NSArray *ringProgressViewsArray;

@end

@implementation JVViewUsageTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _ringProgressViewsArray = @[_tvUsageRingProgressView, _gamesUsageRingProgressView, _computerUsageRingProgressView];
    
    UIColor* tintColor = [UIColor colorWithRed:80/255.f green:130/255.f blue:180/255.f alpha:1];
    UIColor* redColor = [UIColor colorWithRed:255/255.f green:49/255.f blue:10/255.f alpha:1];
    
    for( int i = 0; i < [_ringProgressViewsArray count]; i++ )
    {
        KAProgressLabel *ringProgressView = _ringProgressViewsArray[i];
        
        [ringProgressView setBorderWidth:12.0];
        [ringProgressView setColorTable: @{ NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):i < 2 ? tintColor : [UIColor darkGrayColor], NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor): i < 2 ? [UIColor darkGrayColor] : redColor }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [_tvUsageRingProgressView setProgress:9/12.f timing:TPPropertyAnimationTimingEaseOut duration:0.5 delay:0.25];
    [_gamesUsageRingProgressView setProgress:4/6.f timing:TPPropertyAnimationTimingEaseOut duration:0.5 delay:0.25];
    [_computerUsageRingProgressView setProgress:1.f timing:TPPropertyAnimationTimingEaseOut duration:0.5 delay:0.25];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch( section )
    {
        case 0:
        {
            return 3;
        }
        case 1:
        {
            return 1;
        }
    }
    return 0;
}

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
