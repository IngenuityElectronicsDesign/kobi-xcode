//
//  JVDeviceClassCell.h
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardManager.h"
#import "JVDeviceClassData.h"

@protocol JVDeviceClassCellDelegate <NSObject>

- (void)deviceClassWithIndex:(int)index didChangeName:(NSString*)newName;

@end

@interface JVDeviceClassCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, weak) id<JVDeviceClassCellDelegate> delegate;

@property (nonatomic, getter=isInEditMode) BOOL editMode;
@property (nonatomic) int index;

@property (nonatomic, strong) UIImageView* classIconView;
@property (nonatomic, strong) UIButton* classIconButton;
@property (nonatomic, strong) UILabel* classNameLabel;
@property (nonatomic, strong) UITextField* classNameTextField;

- (void)setupWithDeviceClass:(JVDeviceClassData*)deviceClass index:(int)index andIsInEditMode:(BOOL)isEditing;

@end
