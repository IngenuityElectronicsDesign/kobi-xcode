//
//  JVMenuIconView.h
//  Jeeves
//
//  Created by Simon on 9/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVIconView.h"
#import "XIImageView.h"
#import "XIImage.h"

@interface JVMenuIconView : JVIconView

- (id)initWithFrame:(CGRect)frame title:(NSString*)title image:(UIImage*)image andIndex:(int)index;

@end

