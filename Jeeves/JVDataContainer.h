//
//  JVDataContainer.h
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVData.h"

@interface JVDataContainer : NSObject

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts;

@end
