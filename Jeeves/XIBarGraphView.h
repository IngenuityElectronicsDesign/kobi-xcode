//
//  XIBarGraphView.h
//  eXtended Interface
//
//  Created by Simon on 14/10/13.
//  Copyright (c) 2013 Simon Narai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XIBarGraphView : UIView
{
    
}

- (void)drawBarsInGraphWithDataArray:(NSArray*)dataArray withMaxHeight:(NSNumber *)maxHeight;

@end
