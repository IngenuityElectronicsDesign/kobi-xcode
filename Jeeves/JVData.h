//
//  JVData.h
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kBasicConflict -2
#define kClassTypeConflict -1

@interface JVData : NSObject

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet*)conflicts;

@end
