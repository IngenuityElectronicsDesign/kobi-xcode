//
//  JVTimeWarningViewController.h
//  Jeeves
//
//  Created by Simon on 31/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "MYBlurIntroductionView.h"
#import "MYIntroductionPanel.h"
#import "AMBlurView.h"

@interface JVTimeWarningViewController : UIViewController <MYIntroductionDelegate>

@end
