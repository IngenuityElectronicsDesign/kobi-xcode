//
//  JVPinView.m
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVPinView.h"

#define kPinEntryCharsCount 4
#define kPinButtonsCount 5
#define kKeyDiameter 72
#define kKeySpacing 26
#define kDigitDiameter 48
#define kDigitSpacing 12
#define kDigitEdgeSpacing 34
#define kBackgroundHeight 183

char _pinEntryChars[kPinEntryCharsCount];

@interface JVPinView ()

@property (nonatomic, strong) JVPinCodeData* targetPinCode;
@property (nonatomic) int pinEntryIndex;
@property (nonatomic, strong) NSMutableArray* pinDigitInicatorsArray;
@property (nonatomic, strong) NSMutableArray* pinButtonsArray;

@end

@implementation JVPinView

- (id)initWithFrame:(CGRect)frame isOverriding:(BOOL)isOverriding
{
    if( self = [super initWithFrame:frame] )
    {
        _overriding = isOverriding;
        
        // Setup Digit Indictors
        _pinDigitInicatorsArray = [[NSMutableArray alloc] init];
        
        for( int i = 0; i < kPinEntryCharsCount; i++ )
        {
            JVPinDigitIndicatorControl* digit = [[JVPinDigitIndicatorControl alloc] initWithFrame:CGRectMake(kDigitEdgeSpacing+kDigitSpacing+((kDigitSpacing+kDigitDiameter)*i), 0, kDigitDiameter, kDigitDiameter)];
            [_pinDigitInicatorsArray addObject:digit];
            [self addSubview:digit];
        }
        
        // Setup Buttons Background View
        UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, [self frame].size.height-kBackgroundHeight, [self frame].size.width, kBackgroundHeight)];
        [backgroundView setBackgroundColor:[UIColor colorWithWhite:0.95f alpha:0.95f]];
        [self addSubview:backgroundView];
        
        _pinButtonsArray = [[NSMutableArray alloc] init];
        
        // Setup Pin Buttons
        for( int i = 0; i < kPinButtonsCount; i++ )
        {
            UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake(((kKeyDiameter+kKeySpacing)*(i%3))+kKeySpacing, [self frame].size.height-kBackgroundHeight + (kKeyDiameter+kKeySpacing*0.5f)*(i/3)+kKeySpacing*0.5f, kKeyDiameter, kKeyDiameter)];
            [button circularPinStyle];
            
            if( i == 2 )
            {
                [button setTitle:@"⌫" forState:UIControlStateNormal];
                [button setTag:0];
            }
            else
            {
                [button setTitle:( i < 2 ? [NSString stringWithFormat:@"%d", i+1] : [NSString stringWithFormat:@"%d", i]) forState:UIControlStateNormal];
                [button setTag:( i < 2 ? i+1 : i )];
            }
            
            [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
            
            [_pinButtonsArray addObject:button];
            [self addSubview:button];
        }
        
        // Pin Code Data
        _targetPinCode = nil;
        _pinEntryIndex = 0;
        
        for( int i = 0; i < kPinEntryCharsCount; i++ )
        {
            _pinEntryChars[i] = -1;
        }
    }
    return self;
}

- (void)setTargetPinCode:(JVPinCodeData *)targetPinCode
{
    _targetPinCode = targetPinCode;
}

- (void)buttonPressed:(UIButton*)button
{
    if( [button tag] != 0 )
    {
        [self writePinDigitWithValue:(int)[button tag]];
    }
    else
    {
        [self erasePinDigit];
    }
}

// Handle PinCode Entry
- (void)writePinDigitWithValue:(int)value
{
    if( _targetPinCode == nil && !_overriding )
    {
        NSLog(@"Error PinView: No Pin Code Set");
        return;
    }
    if( _pinEntryChars[_pinEntryIndex] == -1 )
    {
        _pinEntryChars[_pinEntryIndex] = value + 0x30;
        [_pinDigitInicatorsArray[_pinEntryIndex] setEnabled:YES];
        _pinEntryIndex++;
        
        if( _pinEntryIndex >= kPinEntryCharsCount )
        {
            if( !_overriding )
            {
                if( [_targetPinCode isEqualToPinCode:[[JVPinCodeData alloc] initWithPinString:@(_pinEntryChars)]])
                {
                    [[self delegate] didEnterPinCodeWithSuccess:YES];
                }
                else
                {
                    [[self delegate] didEnterPinCodeWithSuccess:NO];
                }
            }
            else
            {
                if( _overridingPinCode == nil )
                {
                    _overridingPinCode = [[JVPinCodeData alloc] initWithPinString:@(_pinEntryChars)];
                    [[self delegate] didEnterFirstNewPin:[[JVPinCodeData alloc] initWithPinString:@(_pinEntryChars)]];
                }
                else
                {
                    BOOL success = [_overridingPinCode isEqualToPinCode:[[JVPinCodeData alloc] initWithPinString:@(_pinEntryChars)]];
                    [[self delegate] didEnterSecondNewPinWithSuccess:success];
                }
            }
        }
    }
}

- (void)erasePinDigit
{
    _pinEntryIndex--;
    if( _pinEntryIndex <= 0 )
    {
        _pinEntryIndex = 0;
    }
    
    if( _pinEntryChars[_pinEntryIndex] != -1 )
    {
        _pinEntryChars[_pinEntryIndex] = -1;
        [_pinDigitInicatorsArray[_pinEntryIndex] setEnabled:NO];
    }
}

- (void)resetPinDigits
{
    for( int i = 0; i < kPinEntryCharsCount; i++ )
    {
        _pinEntryChars[i] = -1;
        [_pinDigitInicatorsArray[i] setEnabled:NO];
    }
    
    _pinEntryIndex = 0;
}

- (void)resetPinDigitsAndOverride
{
    [self resetPinDigits];
    _overridingPinCode = nil;
}

#pragma mark - Getters and Setters

- (void)setTintColor:(UIColor *)tintColor
{
    for( JVPinDigitIndicatorControl* digitIndicator in _pinDigitInicatorsArray )
    {
        [digitIndicator setTintColor:tintColor];
    }
    for( UIButton* button in _pinButtonsArray )
    {
        [button setTintColor:tintColor];
    }
}

@end
