//
//  JVUserSelectionCell.h
//  Kobi
//
//  Created by Simon on 17/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XIImage.h"
#import "MultiSelectSegmentedControl.h"

@protocol JVUserSelectionCellDelegate <NSObject>

- (void)userSelectionDidChange;

@end

@interface JVUserSelectionCell : UITableViewCell <MultiSelectSegmentedControlDelegate>

@property (nonatomic, weak) id<JVUserSelectionCellDelegate> delegate;

@end
