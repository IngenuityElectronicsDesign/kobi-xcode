//
//  JVPinViewController.h
//  Jeeves
//
//  Created by Simon on 10/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVPinView.h"

@interface JVPinViewController : UIViewController <JVPinViewDelegate>

@property (nonatomic, strong) JVPinCodeData* pinCode;

- (void)passPinCode:(JVPinCodeData*)pinCode;

@end
