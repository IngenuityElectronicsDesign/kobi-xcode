//
//  JVDeviceClassSelectionCell.m
//  Kobi
//
//  Created by Simon on 8/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVDeviceClassSelectionCell.h"

@interface JVDeviceClassSelectionCell ()

@property (nonatomic, strong) MultiSelectSegmentedControl* segmentedControl;

@end

@implementation JVDeviceClassSelectionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if( self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] )
    {
        _segmentedControl = [[MultiSelectSegmentedControl alloc] initWithFrame:CGRectMake(20, 8, 280, 29)];
        
        for( int i = 0; i < [[[JVShared s] deviceClassDataContainer] count]; i++ )
        {
            JVDeviceClassData* device = [[[JVShared s] deviceClassDataContainer] deviceClassForIndex:i];
            
            [_segmentedControl insertSegmentWithTitle:[device name] atIndex:i animated:NO];
        }
        
        [_segmentedControl setSelectedSegmentIndexes:[[JVShared s] selectedDeviceClassesIndexSet]];
        [_segmentedControl setTintColor:[[JVShared s] systemTintColor]];
        [_segmentedControl setDelegate:self];
        [self addSubview:_segmentedControl];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:NO];
}

#pragma mark - TabView Delegate

- (void)multiSelect:(MultiSelectSegmentedControl *)multiSelecSegmendedControl didChangeValue:(BOOL)value atIndex:(NSUInteger)index
{
    
}

- (void)multiSelect:(MultiSelectSegmentedControl *)multiSelectSegmentedControl indexSetDidChange:(NSIndexSet *)indexSet
{
    [[[JVShared s] selectedDeviceClassesIndexSet] removeAllIndexes];
    [[[JVShared s] selectedDeviceClassesIndexSet] addIndexes:indexSet];
    [[self delegate] deviceClassSelectionDidChange];
}

@end
