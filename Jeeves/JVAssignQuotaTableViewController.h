//
//  JVAssignQuotaTableViewController.h
//  Jeeves
//
//  Created by Simon on 28/10/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVDataSyncTableViewController.h"
#import "JVUserSelectionCell.h"
#import "JVDeviceClassSelectionCell.h"
#import "JVTimeInputCell.h"

@interface JVAssignQuotaTableViewController : JVDataSyncTableViewController <UITableViewDelegate, UITableViewDataSource, JVUserSelectionCellDelegate, JVDeviceClassSelectionCellDelegate, JVTimeInputCellDelegate>

@property (strong, nonatomic) IBOutlet UISegmentedControl *globalTimePeriodSegmentedControl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *kobiModeSegmentedControl;

@property (strong, nonatomic) IBOutlet UILabel *emergencyHoursEnabledLabel;

@property (nonatomic) int selectedDataSetsCount;

@property (nonatomic) BOOL needsSaving;

@property (nonatomic, strong) NSArray* dayNamesByIndex;

@end
