//
//  JVUsageGraphViewController.h
//  Jeeves
//
//  Created by Simon on 14/10/13.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XIBarGraphView.h"

@interface JVUsageGraphViewController : UIViewController

@property (weak, nonatomic) IBOutlet XIBarGraphView *barGraphView;

@end
