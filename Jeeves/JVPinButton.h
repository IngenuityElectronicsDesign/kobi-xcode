//
//  JVPinButton.h
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (JVButton)

- (void)circularPinStyle;

@end
