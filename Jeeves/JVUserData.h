//
//  JVUser.h
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JVData.h"
#import "JVPinCodeData.h"

typedef NS_ENUM(char, UserConflictTypes)
{
    UserConflictType_NAME = 0,
    UserConflictType_PORTRAIT,
    UserConflictType_COLOR,
    UserConflictType_PIN
};

typedef NS_ENUM(char, UserTintColorIndex)
{
    SYSTEM,
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    BLUE,
    PURPLE,
    PINK
};

typedef NS_ENUM(char, ConflictType)
{
    CONFLICT_NAME,
    CONFLICT_COLOR
};

@interface JVUserData : JVData

@property (nonatomic, strong) NSString* name;
@property (nonatomic, getter=isAdmin) BOOL admin;
@property (nonatomic, strong) UIImage* portrait;
@property (nonatomic) UserTintColorIndex tintColorIndex;
@property (nonatomic, strong) JVPinCodeData* pinCode;

- (id)initWithName:(NSString*)name portrait:(UIImage*)portrait andColorIndex:(UserTintColorIndex)colorIndex;
- (id)initWithName:(NSString *)name portrait:(UIImage *)portrait colorIndex:(UserTintColorIndex)colorIndex andIsAdmin:(BOOL)isAdmin;

- (void)setPinCodeWithString:(NSString*)pinCodeString;

- (BOOL)isEqualTo:(JVUserData*)otherUser;
- (BOOL)isConflicted:(JVUserData*)otherUser withType:(ConflictType)type;
- (BOOL)isPinConflicted:(JVPinCodeData*)otherPin;

@end
