//
//  JVPinView.h
//  Jeeves
//
//  Created by Simon on 2/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVPinCodeData.h"
#import "JVPinButton.h"
#import "JVPinDigitIndicatorControl.h"

@protocol JVPinViewDelegate <NSObject>

@optional
- (void)didEnterPinCodeWithSuccess:(BOOL)success;
- (void)didEnterFirstNewPin:(JVPinCodeData*)pinCode;
- (void)didEnterSecondNewPinWithSuccess:(BOOL)success;

@end

@interface JVPinView : UIView

@property (nonatomic, weak) id<JVPinViewDelegate> delegate;
@property (nonatomic, getter=isOverriding) BOOL overriding;

@property (nonatomic, strong) JVPinCodeData* overridingPinCode;

- (id)initWithFrame:(CGRect)frame isOverriding:(BOOL)isOverriding;

- (void)setTargetPinCode:(JVPinCodeData*)targetPinCode;
- (void)resetPinDigits;
- (void)resetPinDigitsAndOverride;

@end
