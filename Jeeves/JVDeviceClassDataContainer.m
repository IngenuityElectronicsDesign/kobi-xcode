//
//  JVDeviceClassDataContainer.m
//  Kobi
//
//  Created by Simon on 15/01/2014.
//  Copyright (c) 2014 Ingenuity Design. All rights reserved.
//

#import "JVDeviceClassDataContainer.h"

@implementation JVDeviceClassDataContainer

- (id)init
{
    if( self = [super init] )
    {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (BOOL)isEqualToOther:(id)otherData withConflictsArray:(NSMutableSet *)conflicts
{
    if( [super isEqualToOther:otherData withConflictsArray:conflicts] )
    {
        if( [otherData class] != [self class] )
        {
            [conflicts addObject:@(kClassTypeConflict)];
            return NO;
        }
        
        BOOL isEqual = YES;
        JVDeviceClassDataContainer* other = otherData;
        
        NSMutableSet* conflictsArray = [[NSMutableSet alloc] init];
        
        for( int i = 0; i < [[self dataArray] count]; i++ )
        {
            for( int j = i; j < [[other dataArray] count]; j++ )
            {
                if( i >= [[self dataArray] count] || j >= [[other dataArray] count] )
                {
                    continue;
                }
                
                JVDeviceClassData* thisData = [self dataArray][i];
                JVDeviceClassData* oData = [other dataArray][j];
                
                if( ![thisData isEqualToOther:oData withConflictsArray:conflictsArray] )
                {
                    isEqual = NO;
                }
            }
        }
        
        return isEqual;
    }
    return NO;
}

- (BOOL)addDeviceClassWithName:(NSString*)name andIcon:(UIImage*)icon
{
    if( name != nil && icon != nil && [_dataArray count] < kMaxNumDeviceClasses )
    {
        JVDeviceClassData* data = [[JVDeviceClassData alloc] initWithName:name andIcon:icon];
        [_dataArray addObject:data];
        
        return YES;
    }
    return NO;
}

- (BOOL)removeDeviceClassAtIndex:(NSUInteger)index
{
    if( index < [_dataArray count] )
    {
        [_dataArray removeObjectAtIndex:index];
        
        return YES;
    }
    return NO;
}

#pragma mark - Retrieve Data

- (NSArray*)dataArrayForIndexSet:(NSIndexSet *)indexSet
{
    NSMutableArray* returnArray = [[NSMutableArray alloc] init];
    
    for( int i = 0; i < [_dataArray count]; i++ )
    {
        if( [indexSet containsIndex:i] )
        {
            [returnArray addObject:_dataArray[i]];
        }
    }
    
    return [NSArray arrayWithArray:returnArray];
}

- (JVDeviceClassData*)deviceClassForIndex:(NSUInteger)index
{
    if( index < [_dataArray count] )
    {
        return (JVDeviceClassData*)_dataArray[index];
    }
    return nil;
}

#pragma mark - Getters and Setters

- (NSUInteger)count
{
    return [_dataArray count];
}

@end
