//
//  JVIconView.m
//  Jeeves
//
//  Created by Simon on 9/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import "JVIconView.h"

#define kButtonDiameter 96
#define kLabelHeight 21
#define kSpacing 11

@implementation JVIconView

- (id)initWithFrame:(CGRect)frame andIndex:(int)index
{
    if( self = [super initWithFrame:frame] )
    {
        _index = index;
        
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button setFrame:CGRectMake(0, 0, kButtonDiameter, kButtonDiameter)];
        [_button addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_button];
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0, kButtonDiameter+kSpacing, kButtonDiameter, kLabelHeight)];
        [_label setFont:[UIFont systemFontOfSize:17.f]];
        [_label setTextAlignment:NSTextAlignmentCenter];
        [_label setTextColor:[UIColor blackColor]];
        [self addSubview:_label];
    }
    return self;
}

- (void)buttonPressed
{
    [[self delegate] didPressIconWithIndex:_index];
}

@end
