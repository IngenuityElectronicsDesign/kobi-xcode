//
//  JVUserIconView.h
//  Jeeves
//
//  Created by Simon on 4/12/2013.
//  Copyright (c) 2013 Ingenuity Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVIconView.h"
#import "XIImageView.h"
#import "XIImage.h"

@interface JVUserIconView : JVIconView

@property (nonatomic, strong) JVUserData* userData;

- (id)initWithFrame:(CGRect)frame userData:(JVUserData*)userData andIndex:(int)index;
- (id)initWithFrame:(CGRect)frame blankAsAdmin:(BOOL)asAdmin andIndex:(int)index;
- (id)initWithFrame:(CGRect)frame asBlankWithIndex:(int)index;
- (void)setWithUserData:(JVUserData*)userData;
- (void)setBlankAsAdmin:(BOOL)asAdmin;
- (void)setBlank;

@end
